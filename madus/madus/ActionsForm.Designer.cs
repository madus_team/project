﻿namespace madus
{
    partial class ActionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rbAd = new System.Windows.Forms.RadioButton();
            this.rbAntiAd = new System.Windows.Forms.RadioButton();
            this.rbRebel = new System.Windows.Forms.RadioButton();
            this.tbDescribtion = new System.Windows.Forms.TextBox();
            this.btOrder = new System.Windows.Forms.Button();
            this.rbtnNews = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // rbAd
            // 
            this.rbAd.AutoSize = true;
            this.rbAd.Location = new System.Drawing.Point(12, 12);
            this.rbAd.Name = "rbAd";
            this.rbAd.Size = new System.Drawing.Size(70, 17);
            this.rbAd.TabIndex = 0;
            this.rbAd.TabStop = true;
            this.rbAd.Text = "Реклама";
            this.rbAd.UseVisualStyleBackColor = true;
            this.rbAd.CheckedChanged += new System.EventHandler(this.rbAd_CheckedChanged);
            // 
            // rbAntiAd
            // 
            this.rbAntiAd.AutoSize = true;
            this.rbAntiAd.Location = new System.Drawing.Point(12, 58);
            this.rbAntiAd.Name = "rbAntiAd";
            this.rbAntiAd.Size = new System.Drawing.Size(93, 17);
            this.rbAntiAd.TabIndex = 1;
            this.rbAntiAd.TabStop = true;
            this.rbAntiAd.Text = "Антиреклама";
            this.rbAntiAd.UseVisualStyleBackColor = true;
            this.rbAntiAd.CheckedChanged += new System.EventHandler(this.rbAntiAd_CheckedChanged);
            // 
            // rbRebel
            // 
            this.rbRebel.AutoSize = true;
            this.rbRebel.Location = new System.Drawing.Point(12, 81);
            this.rbRebel.Name = "rbRebel";
            this.rbRebel.Size = new System.Drawing.Size(48, 17);
            this.rbRebel.TabIndex = 2;
            this.rbRebel.TabStop = true;
            this.rbRebel.Text = "Бунт";
            this.rbRebel.UseVisualStyleBackColor = true;
            this.rbRebel.CheckedChanged += new System.EventHandler(this.rbRebel_CheckedChanged);
            // 
            // tbDescribtion
            // 
            this.tbDescribtion.Location = new System.Drawing.Point(155, 11);
            this.tbDescribtion.Multiline = true;
            this.tbDescribtion.Name = "tbDescribtion";
            this.tbDescribtion.ReadOnly = true;
            this.tbDescribtion.Size = new System.Drawing.Size(146, 89);
            this.tbDescribtion.TabIndex = 3;
            // 
            // btOrder
            // 
            this.btOrder.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btOrder.Location = new System.Drawing.Point(100, 120);
            this.btOrder.Name = "btOrder";
            this.btOrder.Size = new System.Drawing.Size(93, 45);
            this.btOrder.TabIndex = 4;
            this.btOrder.Text = "Заказать";
            this.btOrder.UseVisualStyleBackColor = true;
            this.btOrder.Click += new System.EventHandler(this.btOrder_Click);
            // 
            // rbtnNews
            // 
            this.rbtnNews.AutoSize = true;
            this.rbtnNews.Location = new System.Drawing.Point(12, 35);
            this.rbtnNews.Name = "rbtnNews";
            this.rbtnNews.Size = new System.Drawing.Size(137, 17);
            this.rbtnNews.TabIndex = 5;
            this.rbtnNews.TabStop = true;
            this.rbtnNews.Text = "Интернет-объявления";
            this.rbtnNews.UseVisualStyleBackColor = true;
            this.rbtnNews.CheckedChanged += new System.EventHandler(this.rbtnNews_CheckedChanged);
            // 
            // ActionsForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(306, 178);
            this.Controls.Add(this.rbtnNews);
            this.Controls.Add(this.btOrder);
            this.Controls.Add(this.tbDescribtion);
            this.Controls.Add(this.rbRebel);
            this.Controls.Add(this.rbAntiAd);
            this.Controls.Add(this.rbAd);
            this.MaximizeBox = false;
            this.Name = "ActionsForm";
            this.Text = "ActionsForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton rbAd;
        private System.Windows.Forms.RadioButton rbAntiAd;
        private System.Windows.Forms.RadioButton rbRebel;
        private System.Windows.Forms.TextBox tbDescribtion;
        private System.Windows.Forms.Button btOrder;
        private System.Windows.Forms.RadioButton rbtnNews;
    }
}