﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace madus
{
    public partial class ActionsForm : Form
    {
        MainForm mainform;
        int j = 0;
        public ActionsForm(MainForm mainform)
        {
            InitializeComponent();
            this.mainform = mainform;
            rbAd.Checked = true;
        }

        private void rbAd_CheckedChanged(object sender, EventArgs e)
        {
            j = 1;
            tbDescribtion.Text = "Цена: 20 000"
                + Environment.NewLine + "Время действия:    2 дня"
                + Environment.NewLine + "Увеличивает имидж игрока каждый день";
        }

        private void rbAntiAd_CheckedChanged(object sender, EventArgs e)
        {
            j = 2;
            tbDescribtion.Text = "Цена: 40 000"
                + Environment.NewLine + "Время действия:    2 дня"
                + Environment.NewLine + "Уменьшает имидж конкурента каждый день";
        }

        private void rbRebel_CheckedChanged(object sender, EventArgs e)
        {
            j = 3;
            tbDescribtion.Text = "Цена: 100 000"
                + Environment.NewLine + "Экипаж конкурента бунтует на ближайшем рейсе, рейс не выполняется";
        }

        private void btOrder_Click(object sender, EventArgs e)
        {
            switch(j)
            {
                case 1: {
                        mainform.order += 2;
                        mainform.updateMoney(-20000, " рекламные услуги");
                    } break;
                case 2: {
                        mainform.order2 -= 2;
                        mainform.updateMoney(-40000, " антирекламные услуги");
                    } break;
                case 3:
                    {
                        mainform.competitorImage -= 0.1;
                        mainform.updateMoney(-100000, " заказ забастовки");
                    } break;
                case 4: {
                        mainform.order += 1;
                        mainform.Image += 0.03;
                        mainform.updateMoney(-35000, " заказ интернет-объявлений");
                    } break;
                default: { }break;

            }
            if(j==1)
            {
                
            }
            else if (j==2)
            {
                
            }
            else if (j == 3)
            {
            }
        }

        private void rbtnNews_CheckedChanged(object sender, EventArgs e)
        {
            j = 4;
            tbDescribtion.Text = "Цена: 35 000"
               + Environment.NewLine + "Время действия:    1 день"
               + Environment.NewLine + "+ разовое повышение имиджа";
        }
    }
}
