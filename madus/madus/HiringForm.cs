﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace madus
{
    public partial class HiringForm : Form
    {
        public MainForm mainform;
        private bool ToFireFrom;
        public double salarychange;
        public HiringForm(MainForm mainform)
        {
            InitializeComponent();
            salarychange = 0;
            this.mainform = mainform;
            this.mainform.InititializeLV(lvWorkers);
            this.mainform.InititializeLV(lvCrew);
            ToFireFrom = true;//значит увольняем с резерва, иначе - с экипажа
            updateHiringLB();
        }
        private void updateHiringLB()
        {
            lvCrew.Items.Clear();
            updatelvWorkers();
            updatelbPlane();
        }
        private void updatelbPlane()
        {
            lbPlanes.Items.Clear();
            string stext = " ";
            for (int i = 0; i < mainform.userPlanes.Count; i++)
            {
                stext = mainform.userPlanes[i].namePlane;
                mainform.userPlanes[i].strikeStatus();
                
                if (!mainform.userPlanes[i].WithoutCrew())
                    stext += " (С экипажем)";
                else if (mainform.userPlanes[i].numOfWorkers > mainform.userPlanes[i].Crew.Count)
                {
                    stext += " (Нужно " + (-mainform.userPlanes[i].Crew.Count + mainform.userPlanes[i].numOfWorkers).ToString() + " чел.)";
                }
                else if (mainform.userPlanes[i].numOfWorkers < mainform.userPlanes[i].Crew.Count)
                {
                    //эта ситуация не должна наступить
                    stext += " (Избыток в "+ (mainform.userPlanes[i].Crew.Count - mainform.userPlanes[i].numOfWorkers).ToString() + " чел.)";
                }
                if (mainform.userPlanes[i].onStrike)
                    stext += " (БУНТ)";
                lbPlanes.Items.Add(stext);
            }
        }
        private void updatelvCrew(Airplane Plane)
        {
            mainform.StrikeWorkers();
            lvCrew.Items.Clear();
            ListViewItem[] arrItems = new ListViewItem[Plane.Crew.Count];
            for (int i = 0; i < Plane.Crew.Count; i++)
            {
                arrItems[i] = new ListViewItem(Plane.Crew[i].id.ToString());
                arrItems[i].SubItems.Add(Plane.Crew[i].salary.ToString());
                switch (Plane.Crew[i].onStrike)
                {
                    case 0: { arrItems[i].SubItems.Add("Отказывается работать"); } break;
                    case 1: { arrItems[i].SubItems.Add("Недоволен"); } break;
                    case 2: { arrItems[i].SubItems.Add("Доволен"); } break;
                    default: { arrItems[i].SubItems.Add(" "); } break;
                }
            }
            lvCrew.Items.AddRange(arrItems);
        }
        private void updatelvWorkers()
        {
            mainform.StrikeWorkers();
            lvWorkers.Items.Clear();
            int countReserve = 0;
            for (int i = 0; i < mainform.userWorkers.Count; i++)
                if (mainform.userWorkers[i].idPlane == 0)
                    countReserve++;
            for (int i = 0; i < mainform.userWorkers.Count; i++)
            {
                if (mainform.userWorkers[i].idPlane == 0)
                {
                    ListViewItem arrItems = new ListViewItem();
                    arrItems = new ListViewItem(mainform.userWorkers[i].id.ToString());
                    arrItems.SubItems.Add(mainform.userWorkers[i].salary.ToString()); //!!!
                    switch (mainform.userWorkers[i].onStrike)
                    {
                        case 0: { arrItems.SubItems.Add("Отказывается работать"); } break;
                        case 1: { arrItems.SubItems.Add("Недоволен"); } break;
                        case 2: { arrItems.SubItems.Add("Доволен"); } break;
                        default: { arrItems.SubItems.Add(" "); } break;
                    }
                    lvWorkers.Items.Add(arrItems);
                }
            }
        }
        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void зарПлатаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToCrewForm hf = new ToCrewForm(this, 0);
            hf.ShowDialog();
            updateHiringLB();
        }

        private void tsmFire_Click(object sender, EventArgs e) 
        {
            int WorkerIndex=-1;
            if (ToFireFrom)
            {
                if (lvWorkers.SelectedIndices.Count == 0)
                {
                    return;
                }
                WorkerIndex = Convert.ToInt32(lvWorkers.SelectedItems[0].SubItems[0].Text);

            }
            else
            {
                if (lvCrew.SelectedIndices.Count == 0)
                {
                    return;
                }
                WorkerIndex = Convert.ToInt32(lvCrew.SelectedItems[0].SubItems[0].Text);
            }
                for (int i=0;i< mainform.userWorkers.Count;i++)
                {
                    if (WorkerIndex == mainform.userWorkers[i].id)
                    {
                        if (!ToFireFrom)
                        {
                            int index = mainform.search(mainform.userWorkers[i].idPlane);
                            mainform.userPlanes[index].Crew.Remove(mainform.userWorkers[i]);
                            mainform.userWorkers.RemoveAt(i);
                            updatelvCrew(mainform.userPlanes[index]);
                    updatelbPlane();
                        }
                        else
                        {
                            mainform.userWorkers.RemoveAt(i);
                            updatelvWorkers();
                        }
                        return;
                    }
                }
        }

        private void lvCrew_SelectedIndexChanged(object sender, EventArgs e)
        {
            ToFireFrom = false;
        }

        private void tsmDelCrew_Click(object sender, EventArgs e)
        {
            if (lbPlanes.SelectedIndex==-1)
            {
                return;
            }
            int index = mainform.search(lbPlanes.SelectedIndex)+1;
            for (int i = 0;i<mainform.userPlanes[index].Crew.Count;i++)
            {
                mainform.userPlanes[index].Crew[i].idPlane = 0;
            }
            mainform.userPlanes[index].Crew.Clear();
            updatelvCrew(mainform.userPlanes[index]);
            updatelvWorkers();
            updatelbPlane();
        } 

        private void tsmToCrew_Click(object sender, EventArgs e)
        {
            if (lbPlanes.SelectedIndex == -1)
            {
                return;
            }
            int index = mainform.search(lbPlanes.SelectedIndex)+1;
            if (mainform.userPlanes[index].numOfWorkers< mainform.userPlanes[index].Crew.Count+1)
            {
                MessageBox.Show("Экипаж данного самолёта уже полон!");
                return;
            }
            int WorkerIndex = -1;
            if (lvWorkers.SelectedIndices.Count == 0)
            {
                return;
            }
            WorkerIndex = Convert.ToInt32(lvWorkers.SelectedItems[0].SubItems[0].Text);
            for (int i = 0; i < mainform.userWorkers.Count; i++)
            {
                if (WorkerIndex == mainform.userWorkers[i].id)
                {
                    mainform.userPlanes[index].Crew.Add(mainform.userWorkers[i]);
                    mainform.userWorkers[i].idPlane = mainform.userPlanes[index].idAirplane;
                    updatelvCrew(mainform.userPlanes[index]);
                    updatelvWorkers();
                    updatelbPlane();
                    lbPlanes.SetSelected(index,true);
                    return;
                }
            }
        }
        private void tsmFromCrew_Click(object sender, EventArgs e)//!
        {
            int WorkerIndex = -1;
            if (lvCrew.SelectedIndices.Count == 0)
            {
                return;
            }
            WorkerIndex = Convert.ToInt32(lvCrew.FocusedItem.SubItems[0].Text);

            for (int i = 0; i < mainform.userWorkers.Count; i++)
            {
                if (WorkerIndex == mainform.userWorkers[i].id)
                {
                    int index = mainform.search(mainform.userWorkers[i].idPlane);
                    mainform.userWorkers[i].idPlane = 0;
                    mainform.userPlanes[index].Crew.Remove(mainform.userWorkers[i]);
                    updatelvCrew(mainform.userPlanes[index]);
                    updatelvWorkers();
                    updatelbPlane();
                    lbPlanes.SetSelected(index, true);
                    return;
                }
            }
        }
        private void lbPlanes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbPlanes.SelectedIndex!=-1)
            updatelvCrew(mainform.userPlanes[mainform.search(lbPlanes.SelectedIndex)+1]);
        }

        private void lvWorkers_SelectedIndexChanged(object sender, EventArgs e)
        {
            ToFireFrom = true;
        }

        private void tsmSalary_Click(object sender, EventArgs e)
        {
            int WorkerIndex = -1;
            if (ToFireFrom)
            {
                if (lvWorkers.SelectedIndices.Count == 0)
                {
                    return;
                }
                WorkerIndex = Convert.ToInt32(lvWorkers.SelectedItems[0].SubItems[0].Text);

            }
            else
            {
                if (lvCrew.SelectedIndices.Count == 0)
                {
                    return;
                }
                WorkerIndex = Convert.ToInt32(lvCrew.SelectedItems[0].SubItems[0].Text);
            }
            for (int i = 0; i < mainform.userWorkers.Count; i++)
            {
                if (WorkerIndex == mainform.userWorkers[i].id)
                {
                    salarychange = mainform.userWorkers[i].salary;
                    ToCrewForm hf = new ToCrewForm(this, 1);
                    hf.ShowDialog();
                    if (salarychange - mainform.userWorkers[i].salary >= 10)
                        mainform.userWorkers[i].rivalStrike = false;
                        mainform.userWorkers[i].salary = salarychange;
                    if (!ToFireFrom)
                    {
                        int index = mainform.search(mainform.userWorkers[i].idPlane);
                        updatelvCrew(mainform.userPlanes[index]);
                        updatelbPlane();
                        lbPlanes.SetSelected(index, true);
                    }
                    else
                    {
                        lvCrew.Items.Clear();
                    }
                    updatelvWorkers();
                    return;
                }
            }
        }
    }
}
