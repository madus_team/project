﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace madus
{
    public enum FlightType //тип рейса (пассажирский/грузовой)
    {
        Passenger = 1,
        Cargo=2
    }

    public enum DirectionType //тип рейса (прямой/обратный)
    {
        Direct = 0,
        Return
    }

    /// <summary>
    ///  Класс Flight, реализующий 
    ///  функционал по работе с рейсами.
    /// </summary>
    public class Flight
    {
        public int IDPlane { get; set; } //id первого самолета в списке самолета-исполнителя рейса    //ОСТАВИЛ, ЧТОБЫ не менять везде код!!!
        public List<Airplane> Airplanes;
        private DateTime departureTimeOne; //время отправления самолета из точки A
        private DateTime arrivalTimeOne;//время прибытия самолета в точку B

        private DateTime departureTimeTwo; //время отправления самолета из точки B
        private DateTime arrivalTimeTwo;//время прибытия самолета в точку A

        private FlightType fType; // тип рейса (грузовой, пассажирский)
        private DirectionType dType; //тип рейса (прямой, обратный)
        private int capacity; // вместимость (либо людей, либо груза)
        private Airport airportFrom;
        private Airport airportTo;
        private int FactCapacity; //вместимость фактическая, то есть сколько людей/груза полететело в конечном итоге
        private double income; //доход
        private Random rand = new Random((int)(DateTime.Now.Ticks));
        public bool ToRemove = false;
        private int regularity; // регулярность (дней)
        private int allPlanesCapacity = 0;
        private bool empty; // пустой ли рейс

        /// <summary>
        /// Метод, определяющий тип рейса: прямой или обратный.
        /// </summary>
        /// <returns>Возвращает тип рейса</returns>
        public DirectionType GenerateTypeOfFlight()
        {
            int result = rand.Next(0, 2);
            if (result != 0)
                DType = DirectionType.Return;
            else
                DType = DirectionType.Direct;
            return DType;
        }

        /// <summary>
        /// Метод, определяющий тип рейса: пассажирский или грузовой.
        /// </summary>
        /// <returns>Возвращает тип рейса</returns>
        public FlightType GenerateTypeOfFlightPass()
        {
            int result = rand.Next(1, 3);
            if (result == 1)
                FType = FlightType.Passenger;
            else
                FType = FlightType.Cargo;
            return FType;
        }


        /// <summary>
        /// Метод, генерирующий требуемую вместимость
        /// для самолета каждого типа в разумном диапазоне.
        /// </summary>
        /// <returns>Возвращает тип рейса</returns>
        private int SetNeseccaryCapacity()
        {
            double coef;

            if (fType == FlightType.Passenger)
                coef = 110;
            else
                coef = 80000;

            return (int)(coef * timeCoef() * (1.0 + rand.Next(-300, 300) / 1000.0));           
            /*//Random rand = new Random();
            int cap = 0;
            switch (FType)
            {
                case FlightType.Passenger:
                    cap = rand.Next(50, 235);
                    break;
                case FlightType.Cargo:
                    cap = rand.Next(6000, 200000);
                    break;
            }
            return cap;*/
        }

        private double timeCoef() // коэффициент в часы пик и антипик
        {
            double time;
            DateTime now = departureTimeOne;
            if (now.TimeOfDay >= new DateTime(1, 1, 1, 7, 0, 0).TimeOfDay &&
                now.TimeOfDay < new DateTime(1, 1, 1, 10, 0, 0).TimeOfDay)
                time = 1.2;
            else
                if (now.TimeOfDay >= new DateTime(1, 1, 1, 22, 0, 0).TimeOfDay ||
                    now.TimeOfDay < new DateTime(1, 1, 1, 1, 0, 0).TimeOfDay)
                    time = 1.2;
                else
                    if (now.TimeOfDay >= new DateTime(1, 1, 1, 14, 0, 0).TimeOfDay &&
                        now.TimeOfDay < new DateTime(1, 1, 1, 17, 0, 0).TimeOfDay)
                        time = 0.8;
                    else
                        time = 1;
            return time;
        }

        /// <summary>
        /// Метод, генерирующий фактическую вместимость
        /// для самолета каждого типа в разумном диапазоне.
        /// </summary>
        /// <returns>Возвращает тип рейса</returns>
        private int GetFactCapacity(double image, double competitorImage) 
        {
            double coef;
            
            if (fType == FlightType.Passenger)
                coef = 170;
            else
                coef = 110000;
            
            return (int)(coef * timeCoef() * (image + 0.5) * (1 + image - competitorImage) *
                    (1.0 + rand.Next(-100, 100) / 1000.0));
            //int cap = rand.Next(1, capacity);
            /*int cap = rand.Next(capacity/2, capacity);
            return cap;*/
        }

        public string Info()
        {
            string infoFlight;
            if (FType == FlightType.Passenger)
            {
                infoFlight = "Тип рейса: пассажирский " + Environment.NewLine + 
                "Вместимость: " + Capacity.ToString() + " чел."+ Environment.NewLine + "Расстояние: " + AirportFrom.GetDistanceBeetwen(AirportTo.AirPoint).ToString("#.") + " км." + Environment.NewLine;
                //"Фактическая вместимость: " +       FactCapacity.ToString() + " чел." + Environment.NewLine;
            }
            else
            {
                infoFlight = "Тип рейса: грузовой " + Environment.NewLine+
                "Грузоподъемность: " + Capacity.ToString() + " тонн" + Environment.NewLine + "Расстояние: " + AirportFrom.GetDistanceBeetwen(AirportTo.AirPoint).ToString("#.") + " км." + Environment.NewLine;
            }
            if (DType == DirectionType.Direct)
            {
                infoFlight += "Направление: прямой " + Environment.NewLine;
            }
            else
            {
                infoFlight += "Направление: обратный " + Environment.NewLine;
            }
            if (regularity != -1)
                infoFlight += "Регулярность (дней): " + regularity + Environment.NewLine;
            infoFlight +="Время отправления из аэропорта: " + AirportFrom.AirportName.ToString() + ": "+ Environment.NewLine+
                DepartureTimeOne.ToString(@"dd\/MM HH:mm") + Environment.NewLine;
            infoFlight += "Время прибытия в аэропорт: " + AirportTo.AirportName.ToString() + ": " + Environment.NewLine +
                ArrivalTimeOne.ToString(@"dd\/MM HH:mm") + Environment.NewLine;

            if (DType == DirectionType.Return)
            {
                infoFlight += "Время отправления из аэропорта: " + AirportTo.AirportName.ToString() + ": " + Environment.NewLine +
                    DepartureTimeTwo.ToString(@"dd\/MM HH:mm") + Environment.NewLine;
                infoFlight += "Время прибытия в аэропорт: " + AirportFrom.AirportName.ToString() + ": " + Environment.NewLine +
                    ArrivalTimeTwo.ToString(@"dd\/MM HH:mm") + Environment.NewLine;
            }
                return infoFlight;
        }


        public string InfoCurrentFlight()
        {
            string infoFlight;
           
            infoFlight = "Номер первого самолёта: " + IDPlane.ToString() + Environment.NewLine;
            if (FType == FlightType.Passenger)
            {
                infoFlight+= "Тип рейса: пассажирский " + Environment.NewLine +
                "Вместимость: " + Capacity.ToString() + " чел." + Environment.NewLine + "Расстояние: " + AirportFrom.GetDistanceBeetwen(AirportTo.AirPoint).ToString("#.") + " км." + Environment.NewLine;
                //"Фактическая вместимость: " +       FactCapacity.ToString() + " чел." + Environment.NewLine;
            }
            else
            {
                infoFlight+= "Тип рейса: грузовой " + Environment.NewLine +
                "Грузоподъемность: " + Capacity.ToString() + " тонн" + Environment.NewLine + "Расстояние: " + AirportFrom.GetDistanceBeetwen(AirportTo.AirPoint).ToString("#.") + " км." + Environment.NewLine;
            }
            if (DType == DirectionType.Direct)
            {
                infoFlight += "Направление: прямой " + Environment.NewLine;
            }
            else
            {
                infoFlight += "Направление: обратный " + Environment.NewLine;
            }
            if (regularity != -1)
                infoFlight += "Регулярность (дней): " + regularity + Environment.NewLine;
            DateTime currentDate = DateTime.Now;
            if (DateTime.Compare(currentDate, DepartureTimeOne) < 0)//время отправления не наступило
                infoFlight += "Время отправления из аэропорта " + AirportTo.AirportName.ToString() + ": " + Environment.NewLine +
                    DepartureTimeTwo.ToString(@"dd\/MM HH:mm") + Environment.NewLine;
            
            else
            if (DateTime.Compare(currentDate, arrivalTimeOne) < 0)
            {//время прибытия не наступило
                infoFlight += "Рейс на исполнении" + Environment.NewLine;
                if (FType == FlightType.Passenger)
                {
                    infoFlight += "На самолет село " +
                    FactCapacity.ToString() + " чел." + Environment.NewLine;
                }
                else
                {
                    infoFlight += "На самолете " +
                    FactCapacity.ToString() + " тонн груза" + Environment.NewLine;
                }
            }
            else
                if (DateTime.Compare(currentDate, arrivalTimeOne) > 0 && DType == DirectionType.Return && DateTime.Compare(currentDate, arrivalTimeTwo) < 0)
            {//время прибытия не наступило
                infoFlight += "Рейс на исполнении" + Environment.NewLine;
                if (FType == FlightType.Passenger)
                {
                    infoFlight += "На самолет село " +
                    FactCapacity.ToString() + " чел." + Environment.NewLine;
                }
                else
                {
                    infoFlight += "На самолете " +
                    FactCapacity.ToString() + " тонн груза" + Environment.NewLine;
                }
            }
            return infoFlight;
        }
        public Flight() { }

        /// <summary>
        /// конструктор пустых рейсов
        /// </summary>
        public Flight(DateTime arrivalTimeOne, Airport airPortFrom, Airport airPortTo, FlightType flightType)
        {
            this.arrivalTimeOne = arrivalTimeOne;
            this.airportFrom = airPortFrom;
            this.airportTo = airPortTo;
            this.fType = flightType;
            this.dType = DirectionType.Direct;
            this.regularity = -1;
            this.capacity = 0;
            //this.FactCapacity = 0;
            this.empty = true;
            GenerateReverseTime();
            //this.income = GetIncome();
            this.Airplanes = new List<Airplane>();
        }

        private void GenerateReverseTime()
        {
            double distance = AirportFrom.GetDistanceBeetwen(AirportTo.AirPoint); //в км
            double time = distance / 1000;//время в пути-в часах

            DepartureTimeOne = ArrivalTimeOne - TimeSpan.FromHours(time);
            DepartureTimeOne -= (TimeSpan.FromSeconds(DepartureTimeOne.Second) + TimeSpan.FromMilliseconds(DepartureTimeOne.Millisecond));
            DepartureTimeTwo = DepartureTimeOne;
            ArrivalTimeTwo = ArrivalTimeOne;
        }

        public Flight(DateTime departureTimeOne, bool regular, List<Airport> airports)
        {
            GenerateAirports(airports);
            if (!regular)
            {
                GenerateTypeOfFlightPass();//пасс или груз
                GenerateTypeOfFlight(); //прям или обрат
                regularity = -1;
            }
            else // регулярные рейсы всегда пассажирские и с возвратом
            {
                fType = FlightType.Passenger;
                dType = DirectionType.Return;
                regularity = (int)rand.Next(1, 8);
            }
            DepartureTimeOne = new DateTime(departureTimeOne.Year, departureTimeOne.Month, departureTimeOne.Day, rand.Next(0, 23), rand.Next(0, 12) * 5, rand.Next(0, 4) * 15);
            GenerateTime();
            Capacity = SetNeseccaryCapacity();
            this.empty = false;
            //FactCapacity = GetFactCapacity();
            //Income = GetIncome();
            this.Airplanes = new List<Airplane>();
        }

        /// <summary>Добавление одного самолета на рейс</summary>
        /// <returns>Был ли уже добавлен этот самолет</returns>
        public bool AddOnePlane(Airplane plane)
        {
            if (!this.Airplanes.Contains(plane))
            {
                if (this.FType == plane.typePlane)
                {
                    this.Airplanes.Add(plane);
                    this.allPlanesCapacity += plane.сapacity;
                    this.IDPlane = this.Airplanes[0].idAirplane;
                }
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Добавление самолетов на рейс
        /// </summary>
        public void AddPlanes(List<Airplane> planes)
        {
            foreach (var item in planes)
            {
                if (!this.Airplanes.Contains(item) && this.FType == item.typePlane)
                {
                    this.Airplanes.Add(item);
                    this.allPlanesCapacity += item.сapacity;
                    this.IDPlane = this.Airplanes[0].idAirplane;
                } 
            }
        }
        /// <summary>
        /// Удаление конкретного самолета с рейса с учетом подъемности
        /// </summary>
        /// <returns>
        /// Произошло ли удаление всех самолетов (вместимость оставшихся меньше требуемой)
        /// </returns>
        public bool RemoveOnePlane(Airplane plane)
        {
            bool result = false;
            Airplane tmp = this.Airplanes.Find(a => a == plane);
            if (tmp != null)
            {
                //если вместимость оставшихся самолетов меньшетребуемой, то удаляются все самолеты
                if ((this.allPlanesCapacity - plane.сapacity) >= this.capacity)
                {
                    this.Airplanes.Remove(tmp);
                    if (this.Airplanes.Count > 0)
                        this.IDPlane = this.Airplanes[0].idAirplane;
                    else
                    {
                        this.IDPlane = 0;
                        result = true;
                    }
                }
                else
                {
                    ClearPlanes();
                    result = true;
                }
            }
            return result;
        }
        /// <summary>
        /// Удаление всех смолетов с рейса
        /// </summary>
        public void ClearPlanes()
        {
            this.Airplanes.Clear(); //не помню остается ли при этом список проинициализированным!!!
            this.IDPlane = 0;
        }

        public double GetIncome(double image, double competitorImage)
        {
            if (!empty)
                factCapacity = Math.Max(capacity, Math.Min(allPlanesCapacity, GetFactCapacity(image, competitorImage)));
            else
                factCapacity = 0;
            double inc = FactCapacity * 2.5 * AirportFrom.GetDistanceBeetwen(AirportTo.AirPoint);
            if (FType == FlightType.Cargo)
                inc /= 1000;
            if (DType == DirectionType.Return)
                inc *= 2;
            income = inc;
            return inc;
        }
        public double GetOutcome(double fuelCost) //Для Debug версии
        {
            //double inc = Capacity * 2.5 * AirportFrom.GetDistanceBeetwen(AirportTo.AirPoint)*0.7;
            //double consumption = 4; // л/км при скорости 1000 км/ч, но это значение сильно отличается в зависимости от самолета
            double inc = fuelCost * AirportFrom.GetDistanceBeetwen(AirportTo.AirPoint); // затраты на топливо
            /*if (FType == FlightType.Cargo)
                inc /= 1000;*/
            if (DType == DirectionType.Direct)
                return inc;
            else
                return inc * 2;
        }

        private void GenerateAirports(List<Airport> airports)
        {
            int indexAirportFrom = rand.Next(0, 5);
            int indexAirportTo = indexAirportFrom;
            AirportFrom = airports.ElementAt(indexAirportFrom);
            while (indexAirportTo == indexAirportFrom)
            {
                indexAirportTo = rand.Next(0, 5);
            }
            AirportTo = airports.ElementAt(indexAirportTo);
        }

        private void GenerateTime()
        {
            double distance = AirportFrom.GetDistanceBeetwen(AirportTo.AirPoint); //в км
            double time = distance / 1000;//время в пути-в часах
            if (DType == DirectionType.Direct)
            {
                ArrivalTimeOne = DepartureTimeOne + TimeSpan.FromHours(time);
                DepartureTimeTwo = DepartureTimeOne;
                ArrivalTimeTwo = ArrivalTimeOne;
            }
            else
            {
                ArrivalTimeOne = DepartureTimeOne + TimeSpan.FromHours(time);
                DepartureTimeTwo = ArrivalTimeOne + TimeSpan.FromHours(1.00);
                ArrivalTimeTwo = DepartureTimeTwo + TimeSpan.FromHours(time);
            }
        }

        public void newRegular() // следующий регулярный рейс       //НЕТ ПРОВЕРКИ НА  ИСТЕЧЕНИЕ  АРЕНДЫ !!!
        {
            departureTimeOne = departureTimeOne.AddDays(regularity);
            GenerateTime();
            //FactCapacity = GetFactCapacity();
        }

        public double Income
        {
            get { return income; }
            set { income = value; }
        }
        public DateTime DepartureTimeOne
        {
            get { return departureTimeOne; }
            set { departureTimeOne = value; }
        }

        public DateTime ArrivalTimeOne
        {
            get { return arrivalTimeOne; }
            set { arrivalTimeOne = value; }
        }

        public DateTime DepartureTimeTwo
        {
            get { return departureTimeTwo; }
            set { departureTimeTwo = value; }
        }

        public DateTime ArrivalTimeTwo
        {
            get { return arrivalTimeTwo; }
            set { arrivalTimeTwo = value; }
        }

        public FlightType FType
        {
            get { return fType; }
            set { fType = value; }
        }
        public DirectionType DType
        {
            get { return dType; }
            set { dType = value; }
        }

        public int Capacity
        {
            get { return capacity; }
            set { capacity = value; }
        }
        public int factCapacity
        {
            get { return FactCapacity; }
            set { FactCapacity = value; }
        }

        public Airport AirportFrom
        {
            get { return airportFrom; }
            set { airportFrom = value; }
        }

        public Airport AirportTo
        {
            get { return airportTo; }
            set { airportTo = value; }
        }

        public int Regularity
        {
            get { return regularity; }
        }

        ~Flight() { }
    }
}