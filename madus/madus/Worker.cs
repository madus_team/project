﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace madus
{
    public class Worker
    {
        private int Id;
        private int IdPlane;
        private int DiscontentLVL; //уровень недовольства
        private double Salary;
        private int OnStrike; //определяет, бастует ли работник 0-бунт, 1-недоволен, 2 - довлен
        private bool RivalStrike;

        public Worker(int Id, int DiscontentLVL, double Salary)
        {
            this.Id = Id;
            this.IdPlane = IdPlane = 0;
            this.DiscontentLVL = DiscontentLVL;
            this.Salary = Salary;
            this.OnStrike = 2;
            RivalStrike = false;
        }
        public Worker(){}
        public void Strike(double rivalSalary, double averageSalary) //метод "забастовка"
        {
            if (rivalStrike)
            {
                onStrike = 0;
                return;
            }
            double sumSal = Salary - rivalSalary;
            double samSal = Salary - averageSalary;
            if (sumSal >= 0)
            {
                onStrike = 2;
            }

            else if (sumSal < 0 && -sumSal > Salary * 0.15)
            {
                onStrike = 0;
                return;
            }
            else
            {
                onStrike = 1;
            }
            if (samSal < 0 && -samSal > Salary * 0.15)
            {
                onStrike = 0;
                return;
            }
            if (samSal<0)
            {
                onStrike = 1;
                return;
            }
            else
            {
                onStrike = 2;
            }


        }
        public int id
        {
            get { return Id; }
        }
        public int idPlane
        {
            get { return IdPlane; }
            set { IdPlane = value; }
        }
        public int discontentLVL
        {
            get { return DiscontentLVL; }
            set { DiscontentLVL = value; }
        }
        public double salary
        {
            get { return Salary; }
            set { Salary = value; }
        }
        public int onStrike
        {
            get { return OnStrike; }
            set { OnStrike = value; }
        }
        public bool rivalStrike
        {
            get { return RivalStrike; }
            set { RivalStrike = value; }
        }
    }
}
