﻿using System.Device.Location;

namespace madus
{
    public class Airport
    {
        private string airportName;
        private Coordinates airPoint;

        public Airport(Coordinates point, string name)
        {
            this.airPoint = point;
            this.airportName = name;
        }

        public string AirportName
        {
            get { return airportName; }
        }

        public Coordinates AirPoint
        {
            get { return airPoint; }
        }

        public struct Coordinates
        {
            public double x, y;
            public Coordinates(double x, double y)
            {
                this.x = x;
                this.y = y;
            }
        }

        public double GetDistanceBeetwen(Coordinates point2)
        {
            double distance = 0.0;
            GeoCoordinate geoPoint1 = new GeoCoordinate(airPoint.x, airPoint.y);
            GeoCoordinate geoPoint2 = new GeoCoordinate(point2.x, point2.y);
            distance = geoPoint1.GetDistanceTo(geoPoint2);
            return distance / 1000;//в км
        }
    }
}
