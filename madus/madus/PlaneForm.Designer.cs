﻿namespace madus
{
    partial class PlaneForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem("");
            System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem("");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PlaneForm));
            this.gbMakers = new System.Windows.Forms.GroupBox();
            this.lbMaker = new System.Windows.Forms.ListBox();
            this.gbMaker = new System.Windows.Forms.GroupBox();
            this.tbMaker = new System.Windows.Forms.TextBox();
            this.gbOperations = new System.Windows.Forms.GroupBox();
            this.tbInfo = new System.Windows.Forms.TextBox();
            this.tbLeasing = new System.Windows.Forms.TextBox();
            this.numLeasing = new System.Windows.Forms.NumericUpDown();
            this.lblCurrentMoney = new System.Windows.Forms.Label();
            this.tbMoney = new System.Windows.Forms.TextBox();
            this.btnSale = new System.Windows.Forms.Button();
            this.btnArenda = new System.Windows.Forms.Button();
            this.btnLising = new System.Windows.Forms.Button();
            this.btnBuy = new System.Windows.Forms.Button();
            this.gbGamer = new System.Windows.Forms.GroupBox();
            this.lvPlanes = new System.Windows.Forms.ListView();
            this.gbPlane = new System.Windows.Forms.GroupBox();
            this.tbPlane = new System.Windows.Forms.TextBox();
            this.gbMakers.SuspendLayout();
            this.gbMaker.SuspendLayout();
            this.gbOperations.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numLeasing)).BeginInit();
            this.gbGamer.SuspendLayout();
            this.gbPlane.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbMakers
            // 
            this.gbMakers.Controls.Add(this.lbMaker);
            this.gbMakers.Controls.Add(this.gbMaker);
            this.gbMakers.Dock = System.Windows.Forms.DockStyle.Left;
            this.gbMakers.Location = new System.Drawing.Point(0, 0);
            this.gbMakers.Name = "gbMakers";
            this.gbMakers.Size = new System.Drawing.Size(280, 551);
            this.gbMakers.TabIndex = 0;
            this.gbMakers.TabStop = false;
            this.gbMakers.Text = "Производитель";
            // 
            // lbMaker
            // 
            this.lbMaker.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbMaker.FormattingEnabled = true;
            this.lbMaker.Location = new System.Drawing.Point(3, 16);
            this.lbMaker.Name = "lbMaker";
            this.lbMaker.Size = new System.Drawing.Size(274, 251);
            this.lbMaker.TabIndex = 3;
            this.lbMaker.SelectedIndexChanged += new System.EventHandler(this.lbMaker_SelectedIndexChanged);
            // 
            // gbMaker
            // 
            this.gbMaker.Controls.Add(this.tbMaker);
            this.gbMaker.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.gbMaker.Location = new System.Drawing.Point(3, 274);
            this.gbMaker.Name = "gbMaker";
            this.gbMaker.Size = new System.Drawing.Size(274, 274);
            this.gbMaker.TabIndex = 4;
            this.gbMaker.TabStop = false;
            this.gbMaker.Text = "Описание самолёта";
            // 
            // tbMaker
            // 
            this.tbMaker.BackColor = System.Drawing.SystemColors.Window;
            this.tbMaker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbMaker.Location = new System.Drawing.Point(3, 16);
            this.tbMaker.Multiline = true;
            this.tbMaker.Name = "tbMaker";
            this.tbMaker.ReadOnly = true;
            this.tbMaker.Size = new System.Drawing.Size(268, 255);
            this.tbMaker.TabIndex = 1;
            this.tbMaker.Text = "Сюда инфа по самолётам производителя";
            // 
            // gbOperations
            // 
            this.gbOperations.Controls.Add(this.tbInfo);
            this.gbOperations.Controls.Add(this.tbLeasing);
            this.gbOperations.Controls.Add(this.numLeasing);
            this.gbOperations.Controls.Add(this.lblCurrentMoney);
            this.gbOperations.Controls.Add(this.tbMoney);
            this.gbOperations.Controls.Add(this.btnSale);
            this.gbOperations.Controls.Add(this.btnArenda);
            this.gbOperations.Controls.Add(this.btnLising);
            this.gbOperations.Controls.Add(this.btnBuy);
            this.gbOperations.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbOperations.Location = new System.Drawing.Point(280, 0);
            this.gbOperations.Name = "gbOperations";
            this.gbOperations.Size = new System.Drawing.Size(154, 551);
            this.gbOperations.TabIndex = 1;
            this.gbOperations.TabStop = false;
            this.gbOperations.Text = "Операции";
            this.gbOperations.Enter += new System.EventHandler(this.gbOperations_Enter);
            // 
            // tbInfo
            // 
            this.tbInfo.Enabled = false;
            this.tbInfo.Location = new System.Drawing.Point(6, 322);
            this.tbInfo.Multiline = true;
            this.tbInfo.Name = "tbInfo";
            this.tbInfo.ReadOnly = true;
            this.tbInfo.Size = new System.Drawing.Size(145, 89);
            this.tbInfo.TabIndex = 9;
            this.tbInfo.Text = "Лизинг - форма кредита\r\nарендуйте самолёт на 1-50\r\nдней. По истечении срока\r\nвам " +
    "будет предоставлена\r\nвозможность выкупа самолёта\r\n";
            // 
            // tbLeasing
            // 
            this.tbLeasing.Location = new System.Drawing.Point(31, 417);
            this.tbLeasing.Name = "tbLeasing";
            this.tbLeasing.ReadOnly = true;
            this.tbLeasing.Size = new System.Drawing.Size(93, 20);
            this.tbLeasing.TabIndex = 8;
            this.tbLeasing.Text = "Кол-во дней:";
            this.tbLeasing.Click += new System.EventHandler(this.tbLeasing_Click);
            // 
            // numLeasing
            // 
            this.numLeasing.Location = new System.Drawing.Point(31, 443);
            this.numLeasing.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.numLeasing.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numLeasing.Name = "numLeasing";
            this.numLeasing.Size = new System.Drawing.Size(93, 20);
            this.numLeasing.TabIndex = 7;
            this.numLeasing.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numLeasing.Click += new System.EventHandler(this.tbLeasing_Click);
            // 
            // lblCurrentMoney
            // 
            this.lblCurrentMoney.AutoSize = true;
            this.lblCurrentMoney.Location = new System.Drawing.Point(40, 16);
            this.lblCurrentMoney.Name = "lblCurrentMoney";
            this.lblCurrentMoney.Size = new System.Drawing.Size(80, 13);
            this.lblCurrentMoney.TabIndex = 5;
            this.lblCurrentMoney.Text = "Текущий счет:";
            // 
            // tbMoney
            // 
            this.tbMoney.Location = new System.Drawing.Point(32, 32);
            this.tbMoney.Name = "tbMoney";
            this.tbMoney.ReadOnly = true;
            this.tbMoney.Size = new System.Drawing.Size(92, 20);
            this.tbMoney.TabIndex = 4;
            this.tbMoney.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnSale
            // 
            this.btnSale.Location = new System.Drawing.Point(31, 239);
            this.btnSale.Name = "btnSale";
            this.btnSale.Size = new System.Drawing.Size(93, 45);
            this.btnSale.TabIndex = 3;
            this.btnSale.Text = "Продажа";
            this.btnSale.UseVisualStyleBackColor = true;
            this.btnSale.Click += new System.EventHandler(this.btnSale_Click);
            // 
            // btnArenda
            // 
            this.btnArenda.Location = new System.Drawing.Point(31, 133);
            this.btnArenda.Name = "btnArenda";
            this.btnArenda.Size = new System.Drawing.Size(93, 45);
            this.btnArenda.TabIndex = 2;
            this.btnArenda.Text = "Аренда";
            this.btnArenda.UseVisualStyleBackColor = true;
            this.btnArenda.Click += new System.EventHandler(this.btnArenda_Click);
            // 
            // btnLising
            // 
            this.btnLising.Location = new System.Drawing.Point(31, 469);
            this.btnLising.Name = "btnLising";
            this.btnLising.Size = new System.Drawing.Size(93, 45);
            this.btnLising.TabIndex = 1;
            this.btnLising.Text = "Лизинг";
            this.btnLising.UseVisualStyleBackColor = true;
            this.btnLising.Click += new System.EventHandler(this.btnLising_Click);
            // 
            // btnBuy
            // 
            this.btnBuy.Location = new System.Drawing.Point(32, 82);
            this.btnBuy.Name = "btnBuy";
            this.btnBuy.Size = new System.Drawing.Size(93, 45);
            this.btnBuy.TabIndex = 0;
            this.btnBuy.Text = "Покупка";
            this.btnBuy.UseVisualStyleBackColor = true;
            this.btnBuy.Click += new System.EventHandler(this.btnBuy_Click);
            // 
            // gbGamer
            // 
            this.gbGamer.Controls.Add(this.lvPlanes);
            this.gbGamer.Controls.Add(this.gbPlane);
            this.gbGamer.Dock = System.Windows.Forms.DockStyle.Right;
            this.gbGamer.Location = new System.Drawing.Point(434, 0);
            this.gbGamer.Name = "gbGamer";
            this.gbGamer.Size = new System.Drawing.Size(300, 551);
            this.gbGamer.TabIndex = 2;
            this.gbGamer.TabStop = false;
            this.gbGamer.Text = "Игрок";
            // 
            // lvPlanes
            // 
            this.lvPlanes.Dock = System.Windows.Forms.DockStyle.Top;
            this.lvPlanes.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1,
            listViewItem2});
            this.lvPlanes.Location = new System.Drawing.Point(3, 16);
            this.lvPlanes.Name = "lvPlanes";
            this.lvPlanes.Size = new System.Drawing.Size(294, 250);
            this.lvPlanes.TabIndex = 5;
            this.lvPlanes.UseCompatibleStateImageBehavior = false;
            this.lvPlanes.View = System.Windows.Forms.View.List;
            this.lvPlanes.SelectedIndexChanged += new System.EventHandler(this.lvPlanes_SelectedIndexChanged);
            // 
            // gbPlane
            // 
            this.gbPlane.Controls.Add(this.tbPlane);
            this.gbPlane.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.gbPlane.Location = new System.Drawing.Point(3, 274);
            this.gbPlane.Name = "gbPlane";
            this.gbPlane.Size = new System.Drawing.Size(294, 274);
            this.gbPlane.TabIndex = 4;
            this.gbPlane.TabStop = false;
            this.gbPlane.Text = "Описание самолёта";
            // 
            // tbPlane
            // 
            this.tbPlane.BackColor = System.Drawing.SystemColors.Window;
            this.tbPlane.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbPlane.Location = new System.Drawing.Point(3, 16);
            this.tbPlane.Multiline = true;
            this.tbPlane.Name = "tbPlane";
            this.tbPlane.ReadOnly = true;
            this.tbPlane.Size = new System.Drawing.Size(288, 255);
            this.tbPlane.TabIndex = 1;
            this.tbPlane.Text = "Сюда инфа по самолётам игрока";
            this.tbPlane.TextChanged += new System.EventHandler(this.tbPlane_TextChanged);
            // 
            // PlaneForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::madus.Properties.Resources.airplane;
            this.ClientSize = new System.Drawing.Size(734, 551);
            this.Controls.Add(this.gbOperations);
            this.Controls.Add(this.gbGamer);
            this.Controls.Add(this.gbMakers);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PlaneForm";
            this.Text = "Приобретение самолётов";
            this.gbMakers.ResumeLayout(false);
            this.gbMaker.ResumeLayout(false);
            this.gbMaker.PerformLayout();
            this.gbOperations.ResumeLayout(false);
            this.gbOperations.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numLeasing)).EndInit();
            this.gbGamer.ResumeLayout(false);
            this.gbPlane.ResumeLayout(false);
            this.gbPlane.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbMakers;
        private System.Windows.Forms.GroupBox gbOperations;
        private System.Windows.Forms.Button btnSale;
        private System.Windows.Forms.Button btnArenda;
        private System.Windows.Forms.Button btnLising;
        private System.Windows.Forms.Button btnBuy;
        private System.Windows.Forms.GroupBox gbGamer;
        private System.Windows.Forms.ListBox lbMaker;
        private System.Windows.Forms.GroupBox gbMaker;
        private System.Windows.Forms.TextBox tbMaker;
        private System.Windows.Forms.GroupBox gbPlane;
        private System.Windows.Forms.TextBox tbPlane;
        public System.Windows.Forms.ListView lvPlanes;
        private System.Windows.Forms.TextBox tbInfo;
        private System.Windows.Forms.TextBox tbLeasing;
        private System.Windows.Forms.NumericUpDown numLeasing;
        private System.Windows.Forms.Label lblCurrentMoney;
        private System.Windows.Forms.TextBox tbMoney;
    }
}