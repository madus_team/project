﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace madus
{
    public partial class MainForm : Form
    {
        #region Объявление глобальный переменных
        int week;
        Random rnd;
        int idWorker;
        int idPlane;
        int idFlight;
        DateTime dt;
        int fuelDay;
        bool GameOver;
        int maxDist;
        double GameDay;
        double Salary; //*зарплата работника для генерации
        int DLVL; //*текущий уровень недовольства, генерируемых работников...
        double TimeCoef;
        public double Money;
        public double Image;
        public double competitorImage;
        public int order;
        public int order2;
        public List<String[]> planes;//тут хранится список самолетов, считаный из текстового файла
        public Dictionary<string, double> oldPlanesPrices;//для хранения старых цен для сравнения в конце недели
                                                          //если у вас есть другой вариант напишите мне (Настя)
        public List<Airplane> userPlanes;//список самолетов пользователя
        public List<Airplane> shopPlanes;//список самолетов в магазине
        //public List<Airplane> gamePlanes;//список самолетов в магазине
        public List<Airplane> buyedPlanes;//купленные самолеты на сборке.
        List<Airport> airports;
        public List<Flight> userFlights; //список выбранных юзером рейсов
        List<Flight> gameFlights; // список всех рейсов
        public List<Worker> userWorkers; //работники авиакомпании игрока, кто в резерве
        Queue<string> info; // информация об изменении счета
        int tick = 0; // тик таймера
        public double rivalAverageSalary;
        List<int> botsShopping;
        double stability = 0;//стабильность выполнения рейсов
        int NumFlights = 0;//кол подряд выполненный рейсов
        double reclame = 0; double rivalReclame = 0;
        #endregion

        public MainForm()
        {
            InitializeComponent();
            InitializeMainForm();
        }
        private void InitializeMainForm()
        {
            ClearAllForms();
            //Инициализация переменных
            idPlane = 1;
            idWorker = 1;
            idFlight = 1;
            TimeCoef = 1;
            GameDay = 10;
            fuelDay = 0;
            order = 0;

            rivalAverageSalary = 50;
            DLVL = 0;
            Salary = 50;
            maxDist = 10001; //временная константа (используется присоздании новых самолётов, то есть те, которые мы покупаем, уже имеют километраж)
            Money = 1000000;
            Image = 0.5;
            competitorImage = 0.5; // ПОПРАВИТЬ НА 0.5 ПЕРЕД РЕЛИЗОМ
            GameOver = false;
            planes = new List<String[]>();
            oldPlanesPrices = new Dictionary<string, double>();
            airports = new List<Airport>();
            userFlights = new List<Flight>();
            //gamePlanes = new List<Airplane>();
            gameFlights = new List<Flight>();
            userPlanes = new List<Airplane>();
            shopPlanes = new List<Airplane>();
            buyedPlanes = new List<Airplane>();
            userWorkers = new List<Worker>();
            info = new Queue<string>();
            rnd = new Random((int)(DateTime.Now.Ticks));
            botsShopping = new List<int>();

            //Генерация первых работников
            for (int i = 0; i < 4; i++)
            {
                userWorkers.Add(GenWorkersAtFirstTime());
            }

            //Запуск таймера
            week = 1;
            DateTime ndt = DateTime.Now;
            tsmTimer.Text = new DateTime(ndt.Year, ndt.Month, ndt.Day, 0, 0, 0).ToString();
            setTimerInterval();
            GameDay = 10;
            TimeCoef = 1;
            dt = DateTime.Today;
            работаСоВременемToolStripMenuItem.Visible = false;

            //Рейсы и самолёты по умолчанию
            ReadFiles();
            for (int i = 0; i < 2; i++) // регулярные рейсы
            {
                DateTime randomDate = GetRandomDate(dt, dt.AddDays(7));
                gameFlights.Add(new Flight(randomDate, true, airports));
                Thread.Sleep(1);
            }
            FillGameFlights();
            InititializeLV(lvFlights);
            InititializeLV(lvState);
            FillLbFlight();
            FillLbUserFlight();
            tsmFuelPrice.Text = Math.Round(fuelPrice(fuelDay), 2).ToString();
            tsmImageValue.Text = ((int)(Image * 100)).ToString();
            tsmCompetitorImage.Text = ((int)(competitorImage * 100)).ToString(); // УБРАТЬ ПЕРЕД РЕЛИЗОМ ВМЕСТЕ С ПУНКТОМ МЕНЮ
            tsmMoney.Text = Math.Round(Money, 2).ToString();
            tsmInfo.Text = "";
            Airplane PL = new Airplane();
            Airplane PL2 = new Airplane();

            PL = addPlane(idPlane);
            idPlane++;
            lvPlanes.Items.Add(PL.namePlane);
            userPlanes.Add(PL);
            userPlanes[0].startDate = DateTime.Parse(tsmTimer.Text);
            PL.rent = true;
            PL2 = addPlane(idPlane);
            idPlane++;
            lvPlanes.Items.Add(PL2.namePlane);
            userPlanes.Add(PL2);

            PL = addPlane(idPlane);
            idPlane++;
            shopPlanes.Add(PL);
            PL2 = addPlane(idPlane);
            idPlane++;
            shopPlanes.Add(PL2);
            //gamePlanes.Add(PL2);
            SelectNoPlane();

            for (int i = 0; i < userPlanes[userPlanes.Count - 1].numOfWorkers; i++)//наполнил один самолёт работниками, для теста
            {
                userPlanes[userPlanes.Count - 1].Crew.Add(GenWorkersAtFirstTime());
                userPlanes[userPlanes.Count - 1].Crew[i].idPlane = userPlanes[userPlanes.Count - 1].idAirplane;
            }
            userWorkers.AddRange(userPlanes[userPlanes.Count - 1].Crew);
        }
        private void ClearAllForms()
        {
            lvFlights.Items.Clear();
            lvPlanes.Items.Clear();
            //lvPlanes.Clear();
            tbFlight.Clear();
            tbPlane.Clear();
            tbState.Clear();
            lvState.Items.Clear();
        }
        public Worker GenWorkersAtFirstTime()//для первой генерации работников
        {
            idWorker++;
            return new Worker(idWorker - 1, DLVL, Salary);
        }
        public Worker GenWorkers(double factSalary)//для генерации работников
        {
            idWorker++;
            return new Worker(idWorker - 1, DLVL, factSalary);
        }
        public double money
        {
            get { return Money; }

            set { Money = Math.Round(value, 2); }
        }
        public void StrikeWorkers()//проверка забастовки, вызывать после изменений в работниках и конкуренте(rivalAverageSalary)
        {
            double averageSalary = PaySalaryToWorkers() / userWorkers.Count();
            for (int i = 0; i < userWorkers.Count; i++)
            {
                userWorkers[i].Strike(rivalAverageSalary, averageSalary);
            }
        }

        public void updateMainForm()
        {
            tsmCompetitorImage.Text = (competitorImage * 100).ToString();
            tsmImageValue.Text = (Image * 100).ToString();

            lvFlights.Items.Clear();
            FillLbFlightByOLd();


            //обновление mainform
            lvPlanes.Items.Clear();
            for (int i = 0; i < userPlanes.Count; i++)
            {
                string stext = userPlanes[i].namePlane;
                if (userPlanes[i].inAir)
                    stext += " (На исполнении рейса)";
                userPlanes[i].strikeStatus();
                if (userPlanes[i].onStrike)
                    stext += " (БУНТ)";
                lvPlanes.Items.Add(stext);
            }
            for (int i = 0; i < buyedPlanes.Count; i++)
                lvPlanes.Items.Add(buyedPlanes[i].namePlane + " (Идет сборка)");
            /*for (int i = 0; i < gamePlanes.Count; i++)
                lvPlanes.Items.Add(gamePlanes[i].namePlane + " (На исполнении рейса)");*/
            //for (int i = userPlanes.Count; i < lvPlanes.Items.Count; i++)
            //  lvPlanes.Items[i].ForeColor= Color.Red;
            tsmMoney.Text = Math.Round(Money, 2).ToString();

            FillLbUserFlight();
        }
        private void SelectNoPlane()//выделение красным рейсы к которым нет самолетов
        {
            bool temp = false;
            for (int i = 0; i < gameFlights.Count; i++)
            {
                temp = false;
                for (int j = 0; j < userPlanes.Count; j++)
                {
                    if (gameFlights[i].FType == userPlanes[j].typePlane && (gameFlights[i].Capacity <= userPlanes[j].сapacity) &&
                       (gameFlights[i].AirportFrom.GetDistanceBeetwen(gameFlights[i].AirportTo.AirPoint) <= userPlanes[j].distance))

                        if ((userPlanes[j].leasing && userPlanes[j].leasingEndDate > gameFlights[i].ArrivalTimeTwo) || !userPlanes[j].leasing)
                            temp = true;

                }
                if (!temp) lvFlights.Items[i].BackColor = Color.Red;
            }
        }

        /// <summary>
        /// Заполнение списков вывода
        /// </summary>
        private void FillLbFlightByOLd()//для старых данных
        {
            gameFlights.Sort((a, b) => a.DepartureTimeOne.CompareTo(b.DepartureTimeOne)); // сортировка списка по времени отправления
            lvFlights.Items.Clear();
            ListViewItem[] arrItems = new ListViewItem[gameFlights.Count];
            for (int i = 0; i < gameFlights.Count; i++)
            {
                arrItems[i] = new ListViewItem((i + 1).ToString(), 0);
                arrItems[i].SubItems.Add(gameFlights[i].AirportFrom.AirportName.ToString());
                arrItems[i].SubItems.Add(gameFlights[i].AirportTo.AirportName.ToString());
                arrItems[i].SubItems.Add(gameFlights[i].DepartureTimeOne.ToString(@"dd\/MM HH:mm"));
                arrItems[i].SubItems.Add(gameFlights[i].ArrivalTimeTwo.ToString(@"dd\/MM HH:mm"));
            }
            lvFlights.Items.AddRange(arrItems);
            SelectNoPlane();
        }

        private void FillLbFlight()
        {
            gameFlights.Sort((a, b) => a.DepartureTimeOne.CompareTo(b.DepartureTimeOne)); // сортировка списка по времени отправления
            lvFlights.Items.Clear();

            ListViewItem[] arrItems = new ListViewItem[gameFlights.Count];
            for (int i = 0; i < gameFlights.Count; i++)
            {
                arrItems[i] = new ListViewItem((i + 1).ToString(), 0);
                idFlight++;
                arrItems[i].SubItems.Add(gameFlights[i].AirportFrom.AirportName.ToString());
                arrItems[i].SubItems.Add(gameFlights[i].AirportTo.AirportName.ToString());
                arrItems[i].SubItems.Add(gameFlights[i].DepartureTimeOne.ToString(@"dd\/MM HH:mm"));
                arrItems[i].SubItems.Add(gameFlights[i].ArrivalTimeTwo.ToString(@"dd\/MM HH:mm"));
            }
            lvFlights.Items.AddRange(arrItems);
            //SelectNoPlane();
        }

        private void FillLbUserFlight()//копирование кода, но так быстрее
        {
            userFlights.Sort((a, b) => a.DepartureTimeOne.CompareTo(b.DepartureTimeOne)); // сортировка списка по времени отправления
            lvState.Items.Clear();

            ListViewItem[] arrItems = new ListViewItem[userFlights.Count];
            for (int i = 0; i < userFlights.Count; i++)
            {
                arrItems[i] = new ListViewItem((i + 1).ToString(), 0);
                //idFlight++;
                arrItems[i].SubItems.Add(userFlights[i].AirportFrom.AirportName.ToString());
                arrItems[i].SubItems.Add(userFlights[i].AirportTo.AirportName.ToString());
                if (userFlights[i].FType == FlightType.Passenger)
                    arrItems[i].SubItems.Add("Пассажирский");
                else
                    arrItems[i].SubItems.Add("Грузовой");
                arrItems[i].SubItems.Add(userFlights[i].DepartureTimeOne.ToString(@"dd\/MM HH:mm"));
                arrItems[i].SubItems.Add(userFlights[i].ArrivalTimeTwo.ToString(@"dd\/MM HH:mm"));
                if (userFlights[i].IDPlane != 0)
                    arrItems[i].SubItems.Add("Назначено: " + userFlights[i].Airplanes.Count);
                else
                    arrItems[i].SubItems.Add("Нет");
            }
            lvState.Items.AddRange(arrItems);
            lvState.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            //lvState.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
        }

        public void InititializeLV(ListView lv) //использую в форме с персоналом
        {
            lv.View = View.Details;
            // lv.LabelEdit = true;
            lv.AllowColumnReorder = false;
            lv.AllowDrop = false;
            lv.MultiSelect = false;
            lv.FullRowSelect = true;
            lv.GridLines = true;
        }

        private void lvFlights_ColumnWidthChanging(object sender, ColumnWidthChangingEventArgs e)
        {
            e.Cancel = true;
            e.NewWidth = lvFlights.Columns[e.ColumnIndex].Width;
        }
        private void lvState_ColumnWidthChanging(object sender, ColumnWidthChangingEventArgs e)
        {
            e.Cancel = true;
            e.NewWidth = lvState.Columns[e.ColumnIndex].Width;
        }
        /// <summary>
        /// Рандомайзер для рейсов
        /// </summary>
        private void FillGameFlights()
        {
            DateTime randomDate;
            for (int i = 0; i < 8; i++)
            {
                randomDate = GetRandomDate(DateTime.Parse(tsmTimer.Text), DateTime.Parse(tsmTimer.Text).AddDays(7));
                gameFlights.Add(new Flight(randomDate, false, airports));
                Thread.Sleep(1);
            }
        }

        private DateTime GetRandomDate(DateTime from, DateTime to)
        {
            var range = to - from;
            var randTimeSpan = new TimeSpan((long)(rnd.NextDouble() * range.Ticks));
            return from + randTimeSpan;
        }

        #region Чтение из файлов (первое заполнение рейсов и самолётов)
        public void ReadFiles()
        {
            ReadFileAirports();
            ReadFilePlanes();
        }
        public void ReadFilePlanes()
        {
            try
            {
                using (StreamReader sr = new StreamReader("../../Resources/Plane.txt"))
                {
                    string zagolovok = sr.ReadLine().ToString();
                    int kol = Convert.ToInt32(sr.ReadLine().ToString());
                    for (int i = 0; i < kol; i++)
                    {
                        String line = sr.ReadLine();
                        string[] split = line.Split(new Char[] { ',' });
                        planes.Add(split);
                    }
                    TransportToOldPlanesPrices();
                }
            }
            catch (Exception ex)
            {
                //ResultBlock.Text = "Could not read the file";
            }
        }
        private void TransportToOldPlanesPrices()
        {
            oldPlanesPrices.Clear();
            foreach (var plane in planes)
            {
                oldPlanesPrices.Add(plane[1], Convert.ToDouble(plane[4]));
            }
        }
        void ReadFileAirports()
        {
            try
            {
                using (StreamReader sr = new StreamReader("../../Resources/Airport.txt", Encoding.GetEncoding(1251)))
                {
                    int kol = Convert.ToInt32(sr.ReadLine().ToString());
                    for (int i = 0; i < kol; i++)
                    {
                        String line = sr.ReadLine();
                        string[] split = line.Split(new Char[] { ' ' });
                        airports.Add(new Airport(new Airport.Coordinates(Double.Parse(split[1]), Double.Parse(split[2])), split[0]));
                    }
                }
            }
            catch (Exception ex)
            {
                //ResultBlock.Text = "Could not read the file";
            }
        }
        #endregion

        private Airplane addPlane(int idPlane)
        {
            int quantityPlain = planes.Count;
            // Random rnd = new Random();
            int numberPlain = rnd.Next(0, quantityPlain - 1);
            Airplane newPlane = new Airplane((FlightType)Enum.Parse(typeof(FlightType), planes[numberPlain][0]), planes[numberPlain][1], Convert.ToInt32(planes[numberPlain][2]),
                Convert.ToDouble(planes[numberPlain][4]), Convert.ToDouble(planes[numberPlain][3]), idPlane, rnd.Next(0, maxDist),new DateTime(dt.Year,dt.Month,dt.Day));
            newPlane.updateAircraftCost();
            return newPlane;

        }
        private double PaySalaryToWorkers()
        {
            double sum = 0;
            for (int i = 0; i < userWorkers.Count; i++)
            {
                sum += userWorkers[i].salary;
            }
            return sum;
        }
        private void RandomGenerator()
        {
            //Лучше всё сделать внутри классов
        }
        public DateTime getTime()
        {
            return dt;
        }
        private void tsmHelp_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Здесь будет вызов справочного окна");
        }

        private void tsmExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region Реализация таймера
        private void tsmFast_Click(object sender, EventArgs e)
        {
            if (TimeCoef > 0.03)
                TimeCoef /= 2;
            setTimerInterval();
        }
        private void tsmSlow_Click(object sender, EventArgs e)
        {
            if (TimeCoef < 1)
                TimeCoef *= 2;
            setTimerInterval();
        }
        private void setTimerInterval()
        {
            GameTimer.Interval = (int)(1000 / (24 * 60 / (GameDay * 60 * TimeCoef))); // 10 (GameDay) - количество минут на игровой день
        }
        private void updateCostsAir()//уменьшение цены;
        {
            for(int i = 0;i< shopPlanes.Count;i++)
            {
                TimeSpan ahill = shopPlanes[i].CreateDate - dt;
                if (ahill.Days>=2)
                {
                    foreach (var plane in planes)
                    {
                        if (plane[1] == shopPlanes[i].namePlane)
                        {
                            plane[4] = (Convert.ToDouble(plane[4]) * 0.98).ToString();
                        }
                    }
                }
            }
        }
        private void SortPlanes()
        {
            for(int i=0;i<shopPlanes.Count-1;i++)
            {
                for(int j=i+1;j<shopPlanes.Count;j++)
                {
                    if (shopPlanes[i].CreateDate
                        <shopPlanes[j].CreateDate && i!=j)
                    {
                        Airplane tmp = shopPlanes[i];
                        shopPlanes[i] = shopPlanes[j];
                        shopPlanes[j] = shopPlanes[i];
                    }
                }
            }
        }
        private void GameTimer_Tick(object sender, EventArgs e)
        {
            dt = dt.AddMinutes(1);
            if (botsShopping.Count != 0)
            {
                for (int i = botsShopping.Count - 1; i >= 0; i--)
                {
                    if (botsShopping[i] == dt.Hour)
                    {
                        if (gameFlights.Count != 0)
                        {
                            tsmInfo.ForeColor = Color.Red;
                            tsmInfo.Text = "Конкурент взял рейс " + gameFlights[0].AirportFrom.AirportName + "-" + gameFlights[0].AirportTo.AirportName;
                            gameFlights.RemoveAt(0);
                        }
                        botsShopping.RemoveAt(i);
                    }
                }
            }
            if (dt.Hour == 12 && dt.Minute == 0 && dt.Day == 1)
                updateMoney(-1 * PaySalaryToWorkers(), "выдача зар. плат персоналу");
            if (dt.Hour == 9 && dt.Minute == 0)
            {
                if (dt.Day % 2 == 0)//бот скупает самолёты, потом прочую логику сюда
                {
                    SortPlanes();
                    for (int i = shopPlanes.Count - 1; i >= 0; i--)
                    {
                        tsmInfo.ForeColor = Color.Red;
                        tsmInfo.Text = "Конкурент купил " + shopPlanes[i].namePlane;
                        shopPlanes.RemoveAt(i);
                    }
                }
            }
            if (dt.Hour == 0 && dt.Minute == 0)
            {
                updateCostsAir();
                
                if (order > 0)
                {
                    order--;
                    Image += 0.05;
                    reclame -= 0.05;
                }
                else if (order == 0)
                {
                    Image += reclame;
                    reclame = 0;
                }
                else if (order < 0)
                {
                    order++;
                    Image -= 0.05;
                    reclame += 0.05;
                }
                if (order2 > 0)
                {
                    order2--;
                    competitorImage += 0.05;
                    rivalReclame -= 0.05;
                }
                else if (order2 == 0)
                {
                    competitorImage += rivalReclame;
                    rivalReclame = 0;
                }
                else if (order2 < 0)
                {
                    order2++;
                    competitorImage -= 0.05;
                    rivalReclame += 0.05;
                }
                if (dt.Day % 4 == 0)
                {
                    order -= 2;
                }
                if (dt.Day % 5 == 0)
                {
                    order2 += 2;
                }
                NewDay();
                if (dt.Day % 3 == 0) //поднятие ЗП конкурента 
                    rivalAverageSalary += 5;
                if (competitorImage < Image && dt.Day % 5 == 0) //заказ бунта конкурентом
                {
                    int j = rnd.Next(0, userWorkers.Count);
                    for (int i = 0; i < userWorkers.Count; i++)
                    {
                        userWorkers[i].rivalStrike = true;
                    }
                }
            }
            if (dt.DayOfWeek == DayOfWeek.Sunday && dt.Hour == 23 && dt.Minute == 59)
                UpdatePlanesCost();
            tsmTimer.Text = dt.ToLongDateString() + ' ' + dt.ToShortTimeString();
            AvailibleFlights();
            AvailibleStates();
            StrikeWorkers();
            for (int i = 0; i < userPlanes.Count; i++)
            {
                userPlanes[i].strikeStatus();
            }
        }
        private void UpdatePlanesCost()
        {
            double cost = 0.0;
            foreach (var plane in planes)
            {
                oldPlanesPrices.TryGetValue(plane[1], out cost);
                if (Convert.ToDouble(plane[4]) == cost)
                {
                    plane[4] = (Convert.ToDouble(plane[4]) * 0.98).ToString();
                    foreach (var shopplane in shopPlanes)
                    {
                        if (shopplane.namePlane == plane[1])
                            shopplane.aircraftCost *= 0.98;
                    }
                }
            }
            TransportToOldPlanesPrices();
        }
        private void AvailibleFlights()
        {
            bool b = false;
            for (int i = 0; i < gameFlights.Count; i++)
            {
                if (dt >= gameFlights[i].DepartureTimeOne)
                    if (gameFlights[i].Regularity == -1)
                        gameFlights[i].ToRemove = true;
                    else
                    {
                        gameFlights[i].newRegular();
                        b = true;
                    }
            }

            for (int i = gameFlights.Count - 1; i > -1; i--)
            {
                if (gameFlights[i].ToRemove)
                {
                    gameFlights.RemoveAt(i);
                    b = true;
                }
            }
            if (b)
                FillLbFlightByOLd();
        }
        public int search(int id)
        {
            //int j = 0;
            for (int i = 0; i < userPlanes.Count; i++)
            {
                if (userPlanes[i].idAirplane == id)
                    return i;
            }
            return -1;
            //return userPlanes[j];
        }
        private bool planeStrikeSttus(int id)//для if'a
        {
            int _IDplane = search(id);
            if (id != 0)
            {
                if (userPlanes[_IDplane].onStrike || userPlanes[_IDplane].WithoutCrew())
                    return true;
                else return false;
            }
            else
                return false;
        }
        private void AvailibleStates()
        {
            bool b=false;
            bool flag = false;
            bool flag2 = false;
            for (int i = userFlights.Count-1;i>=0;i--)
            {
                if (userFlights[i].Airplanes.Count==0)
                {
                    if (dt.Date == userFlights[i].DepartureTimeOne.Date &&
                            dt.Hour == userFlights[i].DepartureTimeOne.Hour &&
                            dt.Minute == userFlights[i].DepartureTimeOne.Minute)
                    {
                        updateStability(false);//рейс не прошел стабильность нарушилась

                        userFlights[i].GetIncome(Image, competitorImage);
                        updateMoney(-userFlights[i].Income, "неустойка за рейс " +
                            userFlights[i].AirportFrom.AirportName + " - " + userFlights[i].AirportTo.AirportName);
                        if (userFlights[i].Regularity == -1)
                        {
                            userFlights[i].ToRemove = true;
                            flag = true;
                        }
                        else
                            userFlights[i].newRegular();
                        b = true;
                    }
                }
                else if(userFlights[i].Airplanes.Count > 0)
                {
                    for (int j = userFlights[i].Airplanes.Count - 1; j >= 0; j--)
                    {
                        int _IDplane = search(userFlights[i].Airplanes[j].idAirplane);
                        if (planeStrikeSttus(userFlights[i].Airplanes[j].idAirplane) || (userPlanes[_IDplane].curAirport.AirportName != userFlights[i].AirportFrom.AirportName && userPlanes[_IDplane].curAirport.AirportName != "none"))
                        {
                            if (dt.Date == userFlights[i].DepartureTimeOne.Date &&
                            dt.Hour == userFlights[i].DepartureTimeOne.Hour &&
                            dt.Minute == userFlights[i].DepartureTimeOne.Minute)
                            {

                                updateStability(false);//рейс не прошел стабильность нарушилась
                                userFlights[i].GetIncome(Image,competitorImage);
                                updateMoney(-userFlights[i].Income, "неустойка за рейс " +
                                    userFlights[i].AirportFrom.AirportName + " - " + userFlights[i].AirportTo.AirportName);
                                userFlights[i].Airplanes.RemoveAt(j);
                                if (userFlights[i].Regularity == -1)
                                {
                                    if (userFlights[i].Airplanes.Count == 0)
                                    {
                                        userFlights[i].ToRemove = true;
                                        flag = true;
                                    }
                                }
                                else
                                {
                                    userFlights[i].newRegular();
                                    if (userFlights[i].Airplanes.Count == 0)
                                    {
                                        userFlights[i].Airplanes.Clear();
                                    }
                                }
                                b = true;
                            }
                        }
                        else
                        {
                            if (dt >= userFlights[i].DepartureTimeOne && dt < userFlights[i].ArrivalTimeTwo && !userPlanes[_IDplane].inAir)
                            {
                                updateStability(true);//рейс выполняется стабильность улудшается
                                userPlanes[_IDplane].inAir = true;
                                userFlights[i].GetIncome(Image, competitorImage);
                                double change = -userPlanes[_IDplane].maintenancePrice;
                                if (userFlights[i].DType == DirectionType.Return)
                                    change *= 2;
                                updateMoney(change, "обслуживание самолета перед рейсом " +
                                    userFlights[i].AirportFrom.AirportName + " - " + userFlights[i].AirportTo.AirportName);
                                b = true;
                                //назначение аэропорта для нового самолета
                                if (userPlanes[_IDplane].curAirport.AirportName == "none")
                                    userPlanes[_IDplane].curAirport = userFlights[i].AirportFrom;
                                //userFlights[i].ToRemove = false;
                            }
                            if (dt >= userFlights[i].ArrivalTimeTwo)
                            {
                                userPlanes[_IDplane].inAir = false;
                                userPlanes[_IDplane].ageDistance += userFlights[i].AirportFrom.GetDistanceBeetwen(userFlights[i].AirportTo.AirPoint); // пройденная дистанция
                                userPlanes[_IDplane].updateAircraftCost(); //обновляем состояние самолёта
                                if (userFlights[i].Regularity == -1)
                                    userPlanes[_IDplane].curAirport = userFlights[i].AirportTo;

                                userFlights[i].GetIncome(Image, competitorImage);
                                double changeMoney = GetFinalIncome(userFlights[i], userFlights[i].Airplanes[j]);
                                updateMoney(changeMoney,
                                    (changeMoney < 0 ? "убыток" : "доход") + " за рейс " +
                                    userFlights[i].AirportFrom.AirportName + " - " + userFlights[i].AirportTo.AirportName);
                                userFlights[i].Airplanes.RemoveAt(j);
                                if (userFlights[i].Airplanes.Count==0)
                                {
                                    
                                    if (userFlights[i].Regularity == -1)
                                    {
                                        userFlights[i].ToRemove = true;
                                        flag = true;
                                    }
                                    else
                                        userFlights[i].newRegular();
                                }
                                b = true;
                            }
                        }
                    }
                }
            }
            if (flag)
                for (int i = userFlights.Count - 1; i >= 0; i--)
                    if (userFlights[i].ToRemove)
                        userFlights.RemoveAt(i);
            if (b) updateMainForm();
        }



        /// <summary>
        /// Метод, спрашивающий, если нужен пустой перелет самолета.
        /// </summary>
        /// <returns>
        /// Флажок был ли поставлен пустой рейс
        /// </returns>
        private bool FlyToDestination(int itemPlane, int itemFlight)
        {
            bool yes = false;
            if (userFlights[itemFlight].AirportFrom.AirportName != userPlanes[itemPlane].curAirport.AirportName && userPlanes[itemPlane].curAirport.AirportName != "none")
            {
                //пустой рейс с прилетом за 1 минуту
                Flight _flight = new Flight(userFlights[itemFlight].DepartureTimeOne - TimeSpan.FromMinutes(1.0), userPlanes[itemPlane].curAirport, userFlights[itemFlight].AirportFrom, userFlights[itemFlight].FType);
                //ставим рейс на следующую минуту, чтобы при следующем тике самолет уже вылетел; заносим его в список взятых рейсов
                if (_flight.DepartureTimeOne - TimeSpan.FromMinutes(1.0) == dt && !SearchFlightBeforeAir(itemPlane))
                {   
                    GameTimer.Enabled = false;

                    _flight.IDPlane = userPlanes[itemPlane].idAirplane;
                    
                    //проверка, если самолет уже летит (иначе спрашиваем)
                    if (userPlanes[itemPlane].inAir)
                    {
                        Flight _curFlight = SearchFlightInAir(itemPlane);
                        //для проверки с учетом регулярных рейсов
                        string _airport = _curFlight.Regularity == -1 ? _curFlight.AirportTo.AirportName : _curFlight.AirportFrom.AirportName;
                        //если летит в нужный аэропорт и успевает   
                        if (!(_curFlight.ArrivalTimeTwo < userFlights[itemFlight].DepartureTimeOne && _airport == userFlights[itemFlight].AirportFrom.AirportName))   //ВРЕМЯ ПРИЛЕТА ПРОСТО МЕНЬШЕ ВРЕМЕНИ ВЫЛЕТА!!!
                        {
                            MessageBox.Show("Вы опоздали с перемещением этого самолета!");
                        }
                        yes = false;
                    }
                    else
                    {
                        DialogResult dialogres;
                        dialogres = MessageBox.Show("Требуется перелет самолета из " + userPlanes[itemPlane].curAirport.AirportName + " в " + userFlights[itemFlight].AirportFrom.AirportName +
                            Environment.NewLine + "Хотите осуществить ПУСТОЙ перелет (это займет время и потратит ресурсы)?", "Самолет находится в другом аэропорту!", MessageBoxButtons.YesNo);
                        if (dialogres == System.Windows.Forms.DialogResult.No)
                        {
                            yes = false;
        }
                        else
                        {
                            yes = true;
                            //добавление в список и на форму
                            userFlights.Add(_flight);
                            FillLbUserFlight();
                            //выделение цветом
                            int index = 0;
                            for (int i = 0; i < userFlights.Count; i++)
                                if (userFlights[i] == _flight)
                                    index = i;
                            lvState.Items[index].BackColor = Color.Lime; //НЕ УВЕРЕН, что индекс в листвью с 0!!!
                        }
                    }
                    GameTimer.Enabled = true;
                }
            }

            return yes;
        }

        /// <summary>
        /// Метод, ищущий выполняемый рейс по самолету и текущему времени
        /// </summary>
        /// <returns>
        /// Исполняемый самолетом рейс или рейс без атрибутов
        /// </returns>
        private Flight SearchFlightInAir(int itemPlane)
        {
            Flight _flight = new Flight();
            foreach(var item in userFlights)
            {
                int _IDPlane = search(item.IDPlane);
                if (_IDPlane == itemPlane)
                {
                    if (item.DepartureTimeOne < dt && userPlanes[_IDPlane].inAir)
                    {
                        _flight = item;
                    }
                }
            }
            return _flight;
        }

        /// <summary>
        /// Метод, ищущий ближайщий рейс (через 1 мин) по самолету и текущему времени
        /// </summary>
        /// <returns>
        /// Есть рейс или нет
        /// </returns>
        private bool SearchFlightBeforeAir(int itemPlane)
        {
            //Flight _flight = new Flight();
            bool flag = false;
            foreach (var item in userFlights)
            {
                int _IDPlane = search(item.IDPlane);
                if (_IDPlane == itemPlane)
                {
                    if (item.DepartureTimeOne - TimeSpan.FromMinutes(1.0) == dt && userPlanes[_IDPlane].inAir == false)
                    {
                        //_flight = item;
                        flag = true;
                    }
                }
            }
            return flag;
        }

        private void tsmTimer_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("Stop");
        }
        /// <summary>
        /// Обработка остановки таймера
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsmPause_Click(object sender, EventArgs e)
        {
            if (GameTimer.Enabled)
            {
                GameTimer.Enabled = false;
                //tsmPause.Text = ">";
            }
            else
            {
                GameTimer.Enabled = true;
              //  tsmPause.Text = "||";
            }
        }

        /// <summary>
        /// Восстановление изначального течения времени
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsmPlay_Click(object sender, EventArgs e)
        {
            TimeCoef = 1;
            setTimerInterval();
        }

        #endregion

        private void tsmStartGame_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show("Вы действительно хотите начать новую игру? Все несохраненные данные будут утеряны", 
                                 "Новая игра",
                                 MessageBoxButtons.YesNo,
                                 MessageBoxIcon.Question);
            if (result == DialogResult.No)
                return;
            InitializeMainForm();
        }

        private void tsmPlanes_Click(object sender, EventArgs e)
        {
            GameTimer.Enabled = false;
            SortPlanes();
            
            PlaneForm pf = new PlaneForm(this);
            pf.ShowDialog();
            updateMainForm();
            GameTimer.Enabled = true;
            //tsmPause.Text = "||";
        }

        public string outputAirplane(int numberPlanes)
        {
            if (numberPlanes < 0)
                return "";
            /*if (numberPlanes >= (userPlanes.Count + buyedPlanes.Count))
            {
                numberPlanes -= userPlanes.Count + buyedPlanes.Count;
                return gamePlanes[numberPlanes].Info();

            }*/
            if (numberPlanes < userPlanes.Count)
                return userPlanes[numberPlanes].Info();
            else
                return buyedPlanes[numberPlanes - userPlanes.Count].Info();

        }

        private void lvPlanes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvPlanes.SelectedIndices.Count == 0)
                return;
            tbPlane.Clear();
            int numberPlanes = lvPlanes.SelectedIndices[0];
            tbPlane.Text = outputAirplane(numberPlanes);
            if (numberPlanes < userPlanes.Count)
            for (int i = 0; i < userFlights.Count; i++)
                //if (userPlanes[numberPlanes].idAirplane == userFlights[i].IDPlane)
                if (userFlights[i].Airplanes.Contains(userPlanes[numberPlanes]))
                    lvState.Items[i].BackColor = Color.Lime;
                else
                    lvState.Items[i].BackColor = Color.Empty;
        }

        private void lvFlights_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvFlights.SelectedIndices.Count == 0)
                return;
            tbFlight.Clear();
            int numberFlights = lvFlights.SelectedIndices[0];
            if (numberFlights>=0)
            {
            tbFlight.Text = gameFlights[numberFlights].Info();
            }
        }

        private void lvState_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvState.SelectedIndices.Count == 0)
                return;
            tbState.Clear();
            int numberFlights = lvState.SelectedIndices[0];
            if (numberFlights >= 0 && userFlights.Count!=0)
            {
                tbState.Text = userFlights[numberFlights].InfoCurrentFlight();
            }

        }

        private double fuelPrice(int day) // стоимость топлива на 1 км полета
        {
            double basePrice = 35; // стандартная цена топлива 35 руб/км
            return basePrice * (1 + 0.1 * Math.Sin(2.0 * Math.PI * day / 12.0));
        }

        private void NewDay() // la la lay
        {
            botsShopping.Add(rnd.Next(0, 4));
            Thread.Sleep(1);
            botsShopping.Add(rnd.Next(0, 8));
            Thread.Sleep(1);
            botsShopping.Add(rnd.Next(8, 12));
            Thread.Sleep(1);
            botsShopping.Add(rnd.Next(12, 16));
            Thread.Sleep(1);
            botsShopping.Add(rnd.Next(16, 23));

            FillGameFlights();
            InititializeLV(lvFlights);
            FillLbFlight();
            int j = rnd.Next(1, 6);
            for(int i = 0; i<userWorkers.Count;i++)
            {
                userWorkers[i].rivalStrike = false;
            }
            // цена топлива
            double oldFuelPrice = fuelPrice(fuelDay);
            fuelDay = (fuelDay + 1) % 12;
            tsmFuelPrice.Text = Math.Round(fuelPrice(fuelDay), 2).ToString();
            if (oldFuelPrice < fuelPrice(fuelDay))
                tsmFuelPrice.ForeColor = Color.LimeGreen;
            else
                tsmFuelPrice.ForeColor = Color.Red;

            for (int i = 0; i < j; i++)
            {
                shopPlanes.Add(addPlane(idPlane));
                idPlane++;
            }
            // перенос всех купленных самолетов в основной список
            for (int i = 0; i < buyedPlanes.Count; i++)
                userPlanes.Add(buyedPlanes[i]);
            buyedPlanes.Clear();

            // вычет денег за аренду
            for (int i = 0; i < userPlanes.Count; i++)
                if (userPlanes[i].rent)
                {
                    //money -= userPlanes[i].rentPrice;
                    updateMoney(-userPlanes[i].rentPrice, "списание денег за аренду самолета " + userPlanes[i].namePlane);
                }
            // проверка даты лизинга
            int ToRemove = 0;
            for (int i = 0; i < userPlanes.Count; i++)
            {
                
                DateTime dt = DateTime.Parse(tsmTimer.Text);
                dt = new DateTime(dt.Year, dt.Month, dt.Day);
                if (userPlanes[i].leasingEndDate == dt)
                {
                    string message = "Срок лизинга истёк" + Environment.NewLine + "хотите приобрести самолёт " + userPlanes[i].namePlane + "?"
                        + Environment.NewLine + "Цена покупки после лизинга: " + userPlanes[i].leasingPrice;
                    DialogResult resMessage = MessageBox.Show(message, userPlanes[i].namePlane, MessageBoxButtons.YesNo, MessageBoxIcon.None);
                    if (resMessage == DialogResult.Yes)
                    {
                        //money -= userPlanes[i].leasingPrice;
                        updateMoney(-userPlanes[i].leasingPrice, "списание денег за выкуп самолета " + userPlanes[i].namePlane);
                        userPlanes[i].leasing = false;
                    }
                    else
                    {
                        userPlanes[i].toRemove = true;
                        ToRemove++;
                    }
                }

            }
            NewDayLeasingRemove(ToRemove);
            // вычет денег за лизинг
            for (int i = 0; i < userPlanes.Count; i++)
                if (userPlanes[i].leasing)
                {
                    //money -= userPlanes[i].rentPrice;
                    updateMoney(-userPlanes[i].rentPrice, "списание денег за лизинг самолета " + userPlanes[i].namePlane);
                }

            if (money < CostOfAllPlanes() && money <= 0)
            {
                if (GameOver == false)
                {
                    GameTimer.Enabled = false;
                    MessageBox.Show("Нам очень жаль, но вы банкрот", "GAME OVER");
                    this.Close();
                    GameTimer.Enabled = true;
                    GameOver = true;
                }
            }
            
            // if (GameOver == false)
            //   GameOverMess();
            updateMainForm();
            SelectNoPlane();
        }
        private void GameOverMess() //не используется
        {
            if (money < CostOfAllPlanes() && money <= 0)
            {
                MessageBox.Show("Нам очень жаль, но вы банкрот", "GAME OVER");
                this.Close();
            }
            GameOver = true;
        }
        private void NewDayLeasingRemove(int ToRemove)
        {
            while (ToRemove != 0)
            {
                for(int i = 0; i < userPlanes.Count; i++)
                    if (userPlanes[i].leasing && userPlanes[i].toRemove)
                    {
                        userPlanes.RemoveAt(i);
                        updateMainForm();
                        break;
                    }
                ToRemove--;
            }
        
        }
        private double CostOfAllPlanes()
        {
            double sum=0;
            for (int i = 0; i < userPlanes.Count; i++)
            {
                if (!userPlanes[i].rent && !userPlanes[i].leasing)
                    sum += userPlanes[i].aircraftCost;
            }
            return sum;
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            /*if (!GameOver)
            {
                var result = MessageBox.Show("Вы действительно хотите выйти? Все несохраненные данные будут утеряны",
                                     "Выход",
                                     MessageBoxButtons.YesNo,
                                     MessageBoxIcon.Question);
                // If the no button was pressed ...
                if (result == DialogResult.No)
                {
                    e.Cancel = true;
                    return;
                }
            }*/
        }

        private void tsmBlockFlight_Click(object sender, EventArgs e)
        { 
            if (lvFlights.SelectedIndices.Count == 0)
            {
                MessageBox.Show("Вы не выбрали рейс!");
                return;
            }
            int selectesIndex=-1;
            if (lvFlights.SelectedItems.Count > 0)
            {
                selectesIndex = lvFlights.Items.IndexOf(lvFlights.SelectedItems[0]);
            }
            userFlights.Add(gameFlights[selectesIndex]);
            gameFlights.RemoveAt(selectesIndex);
            FillLbFlightByOLd();
            FillLbUserFlight();
        }

        private void tsmBlockPlane_Click(object sender, EventArgs e)
        {
            bool flag = false;
            if (lvState.SelectedIndices.Count == 0)
                return;
            if (lvPlanes.SelectedIndex == -1 || lvPlanes.SelectedIndex >= userPlanes.Count)
            {
                MessageBox.Show("Вы не выбрали самолет!");
                return;
            }
            int curFlight = lvState.SelectedIndices[0];
            int curPlane = lvPlanes.SelectedIndex;
            //теперь может быть несколько самолетов
            //if (userFlights[curFlight].IDPlane == 0 )
                if (userFlights[curFlight].FType == userPlanes[curPlane].typePlane && (userFlights[curFlight].Capacity <= userPlanes[curPlane].сapacity) &&
                       (userFlights[curFlight].AirportFrom.GetDistanceBeetwen(userFlights[curFlight].AirportTo.AirPoint) <= userPlanes[curPlane].distance))

                    if ((userPlanes[curPlane].leasing && userPlanes[curPlane].leasingEndDate > userFlights[curFlight].ArrivalTimeTwo) || !userPlanes[curPlane].leasing)
                    {
                        //проверка на то, что самолет находится в нужном аэропорту или успеет туда прибыть (кроме случая, когда аэропорт самолтеа = none)
                        if (userPlanes[curPlane].curAirport.AirportName == userFlights[curFlight].AirportFrom.AirportName || userPlanes[curPlane].curAirport.AirportName == "none" || userPlanes[curPlane].IsEnableToArriveInTime(userFlights[curFlight].AirportFrom, dt, userFlights[curFlight].DepartureTimeOne))     //прибитие точно в срок или раньше, без времени на перерыв !!!
                        {
                            if (!userFlights[curFlight].AddOnePlane(userPlanes[curPlane]))
                            {
                                //userFlights[curFlight].IDPlane = userPlanes[curPlane].idAirplane;
                                //gamePlanes.Add(userPlanes[curPlane]);
                                //userPlanes.RemoveAt(curPlane);
                                lvState.Items[curFlight].BackColor = Color.Lime;
                                //FillLbUserFlight();
                            }
                            else
                            {
                                MessageBox.Show("Этот самолет уже был назначен на этот рейс!");
                            }
                        }
                        else
                        {
                            MessageBox.Show("Самолет не успеет прибыть в нужный аэропорт к вылету!");
                        }
                        flag = true;
            }
            if (!flag)
            {
                MessageBox.Show("Самолет не подходит для выбранного рейса");
            }
            tbState.Text = userFlights[curFlight].InfoCurrentFlight();
            updateMainForm();

        }

        private double GetFinalIncome(Flight fl, Airplane planes)
        {
            double outcome = 0.0;
            //foreach (var plane in planes)
                outcome += fl.GetOutcome(planes.consumption * fuelPrice(fuelDay));
            return fl.Income - outcome;
        }

        private void cancelPlane() // отзыв самолета
        {
            int curFlight = lvState.SelectedIndices[0];
            int curPlane = lvPlanes.SelectedIndex;
            //userFlights[curFlight].IDPlane = 0;
            userFlights[curFlight].RemoveOnePlane(userPlanes[curPlane]);
            updateMainForm();
        }

        private void tsmCancelPlane_Click(object sender, EventArgs e) // отзыв самолета с рейса
        {
            if (lvState.SelectedIndices.Count == 0)
            {
                MessageBox.Show("Вы не выбрали рейс!");
                return;
            }
            
            int curFlight = lvState.SelectedIndices[0];
            if (userFlights[curFlight].IDPlane == 0)
            {
                MessageBox.Show("На данный рейс не назначен самолет!");
                return;
            }

            if (dt >= userFlights[curFlight].DepartureTimeOne)
            {
                MessageBox.Show("Исполнение рейса уже началось!");
                return;
            }

            if (lvPlanes.SelectedIndices.Count == 0)
            {
                MessageBox.Show("Вы не выбрали самолет!");
                return;
            }
            int curPlane = lvPlanes.SelectedIndex;
            if (!userFlights[curFlight].Airplanes.Contains(userPlanes[curPlane]))
            {
                MessageBox.Show("Выбранный самолет не назначен на этот рейс!");
                return;
            }

            cancelPlane();

            tbState.Text = userFlights[curFlight].InfoCurrentFlight();
            updateMainForm();
        }

        private void tsmCancelFlight_Click(object sender, EventArgs e) // отмена рейса
        {
            if (lvState.SelectedIndices.Count == 0)
            {
                MessageBox.Show("Вы не выбрали рейс!");
                return;
            }

            int curFlight = lvState.SelectedIndices[0];
            if (dt >= userFlights[curFlight].DepartureTimeOne)
            {
                MessageBox.Show("Исполнение рейса уже началось!");
                return;
            }

            //cancelPlane();
            userFlights[curFlight].ClearPlanes();

            gameFlights.Add(userFlights[curFlight]);
            userFlights.RemoveAt(curFlight);
            
            FillLbFlightByOLd();
            FillLbUserFlight();
        }

        public void updateMoney(double cash, string s)
        {
            money += cash;
            tsmMoney.Text = Math.Round(Money, 2).ToString();
            if (cash < 0)
                s = Math.Round(cash, 2) + ": " + s;
            else
                s = "+" + Math.Round(cash, 2) + ": " + s;
            info.Enqueue(s);
            SecTimer.Enabled = true;
            tsmInfo.Visible = true;
        }

        private void SecTimer_Tick(object sender, EventArgs e)
        {
            if (tick == 0 && info.Count == 0)
            {
                SecTimer.Enabled = false;
                tsmInfo.Visible = false;
                tsmInfo.Text = "";
                return;
            }
            if (tick == 0)
            {
                tsmInfo.Text = info.Dequeue();
                if (tsmInfo.Text[0] == '-')
                    tsmInfo.ForeColor = Color.Red;
                else
                    tsmInfo.ForeColor = Color.LimeGreen;
            }
            tick = (tick + 1) % 4;
            }

        private void рынокТрудаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GameTimer.Enabled = false;
            HiringForm hf = new HiringForm(this);
            hf.ShowDialog();
            updateMainForm();
            GameTimer.Enabled = true;
        }

        private void tsmActions_Click(object sender, EventArgs e)
        {
            GameTimer.Enabled = false;
            ActionsForm hf = new ActionsForm(this);
            hf.ShowDialog();
            updateMainForm();
            GameTimer.Enabled = true;
            //tsmPause.Text = "||";
        }

        private void updateImage()
        {
            double oldImage = Image;
            Image =  0.5 + stability +reclame;
            tsmImageValue.Text = ((int)(Image * 100)).ToString();
            if (oldImage < Image)
                tsmImageValue.ForeColor = Color.LimeGreen;
            if (oldImage > Image)
                tsmImageValue.ForeColor = Color.Red;
            if (oldImage == Image)
                tsmImageValue.ForeColor = SystemColors.ControlText;
        }
        private void updateStability(bool flag)
        {
            if (flag)//если выполнили рейс
            {
                if ((stability == 0 || stability == -0.05) || ((stability == 0.05 || stability == -0.1) && NumFlights == 5) ||
                   ((stability == 0.1 || stability == -0.15) && NumFlights == 15) || ((stability == 0.15 || stability == -0.2) && NumFlights == 30))
                {
                    stability += 0.05; NumFlights = 0;
                }
                else NumFlights += 1;
            }
            else //если не выполнили рейс
            {
                stability -= 0.05;
                NumFlights = 0;
            }
            updateImage();
        }
    }
 //   если 0.05 и -0.10 - нужно 10 рейсов
 //   если 0.10 и -0.15 - нужно 30 рейсов
 //   и если 0.15 и -0.20 - нужно 50 рейсов
}
