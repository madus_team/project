﻿namespace madus
{
    partial class HiringForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HiringForm));
            this.gbHiring = new System.Windows.Forms.GroupBox();
            this.lbPlanes = new System.Windows.Forms.ListBox();
            this.gbWorkers = new System.Windows.Forms.GroupBox();
            this.lvCrew = new System.Windows.Forms.ListView();
            this.idWorkerCrew = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SalaryCrew = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.StatusCrew = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.tsmToCrewForm = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmFromCrew = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDelCrew = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmSalary = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmFire = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmToCrew = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lvWorkers = new System.Windows.Forms.ListView();
            this.idWorker = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Salary = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Status = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.gbHiring.SuspendLayout();
            this.gbWorkers.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbHiring
            // 
            this.gbHiring.Controls.Add(this.lbPlanes);
            this.gbHiring.Dock = System.Windows.Forms.DockStyle.Left;
            this.gbHiring.Location = new System.Drawing.Point(0, 0);
            this.gbHiring.Name = "gbHiring";
            this.gbHiring.Size = new System.Drawing.Size(212, 306);
            this.gbHiring.TabIndex = 0;
            this.gbHiring.TabStop = false;
            this.gbHiring.Text = "Самолёты";
            // 
            // lbPlanes
            // 
            this.lbPlanes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbPlanes.FormattingEnabled = true;
            this.lbPlanes.Location = new System.Drawing.Point(3, 16);
            this.lbPlanes.Name = "lbPlanes";
            this.lbPlanes.Size = new System.Drawing.Size(206, 287);
            this.lbPlanes.TabIndex = 1;
            this.lbPlanes.SelectedIndexChanged += new System.EventHandler(this.lbPlanes_SelectedIndexChanged);
            // 
            // gbWorkers
            // 
            this.gbWorkers.Controls.Add(this.lvCrew);
            this.gbWorkers.Dock = System.Windows.Forms.DockStyle.Left;
            this.gbWorkers.Location = new System.Drawing.Point(212, 24);
            this.gbWorkers.Name = "gbWorkers";
            this.gbWorkers.Size = new System.Drawing.Size(394, 282);
            this.gbWorkers.TabIndex = 1;
            this.gbWorkers.TabStop = false;
            this.gbWorkers.Text = "Экипаж самолёта";
            // 
            // lvCrew
            // 
            this.lvCrew.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.idWorkerCrew,
            this.SalaryCrew,
            this.StatusCrew});
            this.lvCrew.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvCrew.Location = new System.Drawing.Point(3, 16);
            this.lvCrew.Name = "lvCrew";
            this.lvCrew.Size = new System.Drawing.Size(388, 263);
            this.lvCrew.TabIndex = 0;
            this.lvCrew.UseCompatibleStateImageBehavior = false;
            this.lvCrew.SelectedIndexChanged += new System.EventHandler(this.lvCrew_SelectedIndexChanged);
            // 
            // idWorkerCrew
            // 
            this.idWorkerCrew.Text = "ID";
            this.idWorkerCrew.Width = 30;
            // 
            // SalaryCrew
            // 
            this.SalaryCrew.Text = "Зар.плата";
            this.SalaryCrew.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.SalaryCrew.Width = 90;
            // 
            // StatusCrew
            // 
            this.StatusCrew.Text = "Состояние";
            this.StatusCrew.Width = 1000;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmToCrewForm,
            this.tsmFromCrew,
            this.tsmDelCrew,
            this.tsmSalary,
            this.tsmFire,
            this.tsmToCrew});
            this.menuStrip1.Location = new System.Drawing.Point(212, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(803, 24);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // tsmToCrewForm
            // 
            this.tsmToCrewForm.Name = "tsmToCrewForm";
            this.tsmToCrewForm.Size = new System.Drawing.Size(109, 20);
            this.tsmToCrewForm.Text = "Объявить набор";
            this.tsmToCrewForm.Click += new System.EventHandler(this.зарПлатаToolStripMenuItem_Click);
            // 
            // tsmFromCrew
            // 
            this.tsmFromCrew.Name = "tsmFromCrew";
            this.tsmFromCrew.Size = new System.Drawing.Size(116, 20);
            this.tsmFromCrew.Text = "Убрать с экипажа";
            this.tsmFromCrew.Click += new System.EventHandler(this.tsmFromCrew_Click);
            // 
            // tsmDelCrew
            // 
            this.tsmDelCrew.Name = "tsmDelCrew";
            this.tsmDelCrew.Size = new System.Drawing.Size(158, 20);
            this.tsmDelCrew.Text = "Расформировать экипаж";
            this.tsmDelCrew.Click += new System.EventHandler(this.tsmDelCrew_Click);
            // 
            // tsmSalary
            // 
            this.tsmSalary.Name = "tsmSalary";
            this.tsmSalary.Size = new System.Drawing.Size(193, 20);
            this.tsmSalary.Text = "Назначить зар.плату работника";
            this.tsmSalary.Click += new System.EventHandler(this.tsmSalary_Click);
            // 
            // tsmFire
            // 
            this.tsmFire.Name = "tsmFire";
            this.tsmFire.Size = new System.Drawing.Size(64, 20);
            this.tsmFire.Text = "Уволить";
            this.tsmFire.Click += new System.EventHandler(this.tsmFire_Click);
            // 
            // tsmToCrew
            // 
            this.tsmToCrew.Name = "tsmToCrew";
            this.tsmToCrew.Size = new System.Drawing.Size(130, 20);
            this.tsmToCrew.Text = "Назначить в экипаж";
            this.tsmToCrew.Click += new System.EventHandler(this.tsmToCrew_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lvWorkers);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(606, 24);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(409, 282);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Работники вне экипажей";
            // 
            // lvWorkers
            // 
            this.lvWorkers.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.idWorker,
            this.Salary,
            this.Status});
            this.lvWorkers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvWorkers.Location = new System.Drawing.Point(3, 16);
            this.lvWorkers.Name = "lvWorkers";
            this.lvWorkers.Size = new System.Drawing.Size(403, 263);
            this.lvWorkers.TabIndex = 1;
            this.lvWorkers.UseCompatibleStateImageBehavior = false;
            this.lvWorkers.SelectedIndexChanged += new System.EventHandler(this.lvWorkers_SelectedIndexChanged);
            // 
            // idWorker
            // 
            this.idWorker.Text = "ID";
            this.idWorker.Width = 30;
            // 
            // Salary
            // 
            this.Salary.Text = "Зар. плата";
            this.Salary.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.Salary.Width = 90;
            // 
            // Status
            // 
            this.Status.Text = "Состояние";
            this.Status.Width = 1000;
            // 
            // HiringForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1015, 306);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbWorkers);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.gbHiring);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "HiringForm";
            this.Text = "Персонал";
            this.gbHiring.ResumeLayout(false);
            this.gbWorkers.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbHiring;
        private System.Windows.Forms.GroupBox gbWorkers;
        private System.Windows.Forms.ListBox lbPlanes;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmFire;
        private System.Windows.Forms.ToolStripMenuItem tsmToCrew;
        private System.Windows.Forms.ToolStripMenuItem tsmToCrewForm;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ToolStripMenuItem tsmFromCrew;
        private System.Windows.Forms.ToolStripMenuItem tsmDelCrew;
        private System.Windows.Forms.ListView lvCrew;
        private System.Windows.Forms.ListView lvWorkers;
        private System.Windows.Forms.ColumnHeader idWorkerCrew;
        private System.Windows.Forms.ColumnHeader SalaryCrew;
        private System.Windows.Forms.ColumnHeader StatusCrew;
        private System.Windows.Forms.ColumnHeader idWorker;
        private System.Windows.Forms.ColumnHeader Salary;
        private System.Windows.Forms.ColumnHeader Status;
        private System.Windows.Forms.ToolStripMenuItem tsmSalary;
    }
}