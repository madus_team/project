﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace madus
{
    public partial class ToCrewForm : Form
    {
        HiringForm hiringForm;
        int mode;
        public ToCrewForm(HiringForm hiringForm, int mode)
        {
            InitializeComponent();
            this.hiringForm = hiringForm;
            this.mode = mode;
            if (mode == 1)
            {
                this.Text = "Назначение зар.платы работника";
                lblNumOfWorkers.Visible = false;
                numWorkers.Visible = false;
                numSalary.Value = Convert.ToInt32(hiringForm.salarychange);
            }
            if (mode == 0)
            {
                this.Text = "Объявление набора";
                lblNumOfWorkers.Visible = true;
                numWorkers.Visible = true;
                numSalary.Value = Convert.ToInt32(hiringForm.mainform.rivalAverageSalary);
            }
        }
        private Random rand = new Random((int)(DateTime.Now.Ticks));

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (mode == 1)
            {
                hiringForm.salarychange = Convert.ToDouble(numSalary.Value);
            }
            if (mode == 0)
            {
                double sumSal = Convert.ToDouble(numSalary.Value) - hiringForm.mainform.rivalAverageSalary;
                
                if (Convert.ToInt32(numWorkers.Value) > 0)
                {
                    if (sumSal>=0)
                    {
                        int j = rand.Next(1, Convert.ToInt32(numWorkers.Value));
                        if (sumSal > Convert.ToDouble(numSalary.Value)*0.15)
                            for (int i = 0; i < Convert.ToInt32(numWorkers.Value); i++)
                            {
                                hiringForm.mainform.userWorkers.Add(hiringForm.mainform.GenWorkers(Convert.ToDouble(numSalary.Value)));
                            }
                        else
                            for (int i = 0; i < j; i++)
                            {
                                hiringForm.mainform.userWorkers.Add(hiringForm.mainform.GenWorkers(Convert.ToDouble(numSalary.Value)));
                            }
                    }

                    else if (sumSal < 0)
                    {
                        //никто не пришёл
                    }
                }
            }
        }
        
    }
}
