﻿namespace madus
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.msMenu = new System.Windows.Forms.MenuStrip();
            this.tsmExample = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmStartGame = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmLoadGame = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmSaveGame = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmExitGame = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmActions = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmExit = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmSeparator = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmHiring = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmFuel = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmFuelPrice = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmImage = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmImageValue = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmCompetitorImage = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmSchet = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmMoney = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmInfo = new System.Windows.Forms.ToolStripMenuItem();
            this.msTime = new System.Windows.Forms.MenuStrip();
            this.работаСоВременемToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmTimer = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmSlow = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmPlay = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmFast = new System.Windows.Forms.ToolStripMenuItem();
            this.gbFlights = new System.Windows.Forms.GroupBox();
            this.lvFlights = new System.Windows.Forms.ListView();
            this.numFlight = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.otkuda = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.kuda = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.timeDep = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.timeArr = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.gbFlight = new System.Windows.Forms.GroupBox();
            this.tbFlight = new System.Windows.Forms.TextBox();
            this.msFlight = new System.Windows.Forms.MenuStrip();
            this.tsmBlockFlight = new System.Windows.Forms.ToolStripMenuItem();
            this.gbPlanes = new System.Windows.Forms.GroupBox();
            this.lvPlanes = new System.Windows.Forms.ListBox();
            this.gbPlane = new System.Windows.Forms.GroupBox();
            this.tbPlane = new System.Windows.Forms.TextBox();
            this.msPlane = new System.Windows.Forms.MenuStrip();
            this.tsmBlockPlane = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmPlanes = new System.Windows.Forms.ToolStripMenuItem();
            this.gbStates = new System.Windows.Forms.GroupBox();
            this.lvState = new System.Windows.Forms.ListView();
            this.num = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.gbState = new System.Windows.Forms.GroupBox();
            this.tbState = new System.Windows.Forms.TextBox();
            this.msState = new System.Windows.Forms.MenuStrip();
            this.tsmCancelPlane = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmCancelFlight = new System.Windows.Forms.ToolStripMenuItem();
            this.GameTimer = new System.Windows.Forms.Timer(this.components);
            this.SecTimer = new System.Windows.Forms.Timer(this.components);
            this.msMenu.SuspendLayout();
            this.msTime.SuspendLayout();
            this.gbFlights.SuspendLayout();
            this.gbFlight.SuspendLayout();
            this.msFlight.SuspendLayout();
            this.gbPlanes.SuspendLayout();
            this.gbPlane.SuspendLayout();
            this.msPlane.SuspendLayout();
            this.gbStates.SuspendLayout();
            this.gbState.SuspendLayout();
            this.msState.SuspendLayout();
            this.SuspendLayout();
            // 
            // msMenu
            // 
            this.msMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmExample,
            this.tsmActions,
            this.tsmHelp,
            this.tsmExit,
            this.tsmSeparator,
            this.tsmHiring,
            this.tsmFuel,
            this.tsmFuelPrice,
            this.tsmImage,
            this.tsmImageValue,
            this.tsmCompetitorImage,
            this.tsmSchet,
            this.tsmMoney,
            this.tsmInfo});
            this.msMenu.Location = new System.Drawing.Point(0, 0);
            this.msMenu.Name = "msMenu";
            this.msMenu.Size = new System.Drawing.Size(1289, 24);
            this.msMenu.TabIndex = 0;
            this.msMenu.Text = "msMenu";
            // 
            // tsmExample
            // 
            this.tsmExample.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmStartGame,
            this.tsmLoadGame,
            this.tsmSaveGame,
            this.toolStripMenuItem2,
            this.tsmExitGame});
            this.tsmExample.Name = "tsmExample";
            this.tsmExample.Size = new System.Drawing.Size(46, 20);
            this.tsmExample.Text = "Игра";
            // 
            // tsmStartGame
            // 
            this.tsmStartGame.Name = "tsmStartGame";
            this.tsmStartGame.Size = new System.Drawing.Size(155, 22);
            this.tsmStartGame.Text = "Начать";
            this.tsmStartGame.Click += new System.EventHandler(this.tsmStartGame_Click);
            // 
            // tsmLoadGame
            // 
            this.tsmLoadGame.Enabled = false;
            this.tsmLoadGame.Name = "tsmLoadGame";
            this.tsmLoadGame.Size = new System.Drawing.Size(155, 22);
            this.tsmLoadGame.Text = "Загрузить";
            // 
            // tsmSaveGame
            // 
            this.tsmSaveGame.Enabled = false;
            this.tsmSaveGame.Name = "tsmSaveGame";
            this.tsmSaveGame.Size = new System.Drawing.Size(155, 22);
            this.tsmSaveGame.Text = "Сохранить";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(152, 6);
            // 
            // tsmExitGame
            // 
            this.tsmExitGame.Name = "tsmExitGame";
            this.tsmExitGame.Size = new System.Drawing.Size(155, 22);
            this.tsmExitGame.Text = "Выйти из игры";
            this.tsmExitGame.Click += new System.EventHandler(this.tsmExit_Click);
            // 
            // tsmActions
            // 
            this.tsmActions.Name = "tsmActions";
            this.tsmActions.Size = new System.Drawing.Size(70, 20);
            this.tsmActions.Text = "Действия";
            this.tsmActions.Click += new System.EventHandler(this.tsmActions_Click);
            // 
            // tsmHelp
            // 
            this.tsmHelp.Enabled = false;
            this.tsmHelp.Name = "tsmHelp";
            this.tsmHelp.Size = new System.Drawing.Size(65, 20);
            this.tsmHelp.Text = "Справка";
            this.tsmHelp.Click += new System.EventHandler(this.tsmHelp_Click);
            // 
            // tsmExit
            // 
            this.tsmExit.Name = "tsmExit";
            this.tsmExit.Size = new System.Drawing.Size(53, 20);
            this.tsmExit.Text = "Выход";
            this.tsmExit.Click += new System.EventHandler(this.tsmExit_Click);
            // 
            // tsmSeparator
            // 
            this.tsmSeparator.Name = "tsmSeparator";
            this.tsmSeparator.Size = new System.Drawing.Size(22, 20);
            this.tsmSeparator.Text = "|";
            // 
            // tsmHiring
            // 
            this.tsmHiring.Name = "tsmHiring";
            this.tsmHiring.Size = new System.Drawing.Size(74, 20);
            this.tsmHiring.Text = "Персонал";
            this.tsmHiring.Click += new System.EventHandler(this.рынокТрудаToolStripMenuItem_Click);
            // 
            // tsmFuel
            // 
            this.tsmFuel.Name = "tsmFuel";
            this.tsmFuel.Size = new System.Drawing.Size(98, 20);
            this.tsmFuel.Text = "Цена топлива:";
            // 
            // tsmFuelPrice
            // 
            this.tsmFuelPrice.Name = "tsmFuelPrice";
            this.tsmFuelPrice.Size = new System.Drawing.Size(45, 20);
            this.tsmFuelPrice.Text = "Price";
            // 
            // tsmImage
            // 
            this.tsmImage.Name = "tsmImage";
            this.tsmImage.Size = new System.Drawing.Size(62, 20);
            this.tsmImage.Text = "Имидж:";
            // 
            // tsmImageValue
            // 
            this.tsmImageValue.Name = "tsmImageValue";
            this.tsmImageValue.Size = new System.Drawing.Size(52, 20);
            this.tsmImageValue.Text = "Image";
            // 
            // tsmCompetitorImage
            // 
            this.tsmCompetitorImage.Name = "tsmCompetitorImage";
            this.tsmCompetitorImage.Size = new System.Drawing.Size(52, 20);
            this.tsmCompetitorImage.Text = "Image";
            // 
            // tsmSchet
            // 
            this.tsmSchet.Name = "tsmSchet";
            this.tsmSchet.Size = new System.Drawing.Size(99, 20);
            this.tsmSchet.Text = "Текущий счёт:";
            // 
            // tsmMoney
            // 
            this.tsmMoney.Name = "tsmMoney";
            this.tsmMoney.Size = new System.Drawing.Size(56, 20);
            this.tsmMoney.Text = "Money";
            // 
            // tsmInfo
            // 
            this.tsmInfo.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tsmInfo.Name = "tsmInfo";
            this.tsmInfo.Size = new System.Drawing.Size(40, 20);
            this.tsmInfo.Text = "Info";
            this.tsmInfo.Visible = false;
            // 
            // msTime
            // 
            this.msTime.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.msTime.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.работаСоВременемToolStripMenuItem,
            this.tsmTimer,
            this.tsmSlow,
            this.tsmPlay,
            this.tsmFast});
            this.msTime.Location = new System.Drawing.Point(0, 553);
            this.msTime.Name = "msTime";
            this.msTime.Size = new System.Drawing.Size(1289, 24);
            this.msTime.TabIndex = 1;
            this.msTime.Text = "msTime";
            // 
            // работаСоВременемToolStripMenuItem
            // 
            this.работаСоВременемToolStripMenuItem.Name = "работаСоВременемToolStripMenuItem";
            this.работаСоВременемToolStripMenuItem.Size = new System.Drawing.Size(132, 20);
            this.работаСоВременемToolStripMenuItem.Text = "Работа со временем";
            // 
            // tsmTimer
            // 
            this.tsmTimer.Name = "tsmTimer";
            this.tsmTimer.Size = new System.Drawing.Size(179, 20);
            this.tsmTimer.Text = "Здесь выводим дату и время ";
            this.tsmTimer.Click += new System.EventHandler(this.tsmTimer_Click);
            // 
            // tsmSlow
            // 
            this.tsmSlow.Name = "tsmSlow";
            this.tsmSlow.Size = new System.Drawing.Size(35, 20);
            this.tsmSlow.Text = "<<";
            this.tsmSlow.Click += new System.EventHandler(this.tsmSlow_Click);
            // 
            // tsmPlay
            // 
            this.tsmPlay.Name = "tsmPlay";
            this.tsmPlay.Size = new System.Drawing.Size(30, 20);
            this.tsmPlay.Text = "x1";
            this.tsmPlay.Click += new System.EventHandler(this.tsmPlay_Click);
            // 
            // tsmFast
            // 
            this.tsmFast.Name = "tsmFast";
            this.tsmFast.Size = new System.Drawing.Size(35, 20);
            this.tsmFast.Text = ">>";
            this.tsmFast.Click += new System.EventHandler(this.tsmFast_Click);
            // 
            // gbFlights
            // 
            this.gbFlights.Controls.Add(this.lvFlights);
            this.gbFlights.Controls.Add(this.gbFlight);
            this.gbFlights.Controls.Add(this.msFlight);
            this.gbFlights.Dock = System.Windows.Forms.DockStyle.Left;
            this.gbFlights.Location = new System.Drawing.Point(0, 24);
            this.gbFlights.Name = "gbFlights";
            this.gbFlights.Size = new System.Drawing.Size(374, 529);
            this.gbFlights.TabIndex = 2;
            this.gbFlights.TabStop = false;
            this.gbFlights.Text = "Рейсы";
            // 
            // lvFlights
            // 
            this.lvFlights.Alignment = System.Windows.Forms.ListViewAlignment.Default;
            this.lvFlights.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.numFlight,
            this.otkuda,
            this.kuda,
            this.timeDep,
            this.timeArr});
            this.lvFlights.Dock = System.Windows.Forms.DockStyle.Top;
            this.lvFlights.Location = new System.Drawing.Point(3, 40);
            this.lvFlights.Name = "lvFlights";
            this.lvFlights.Size = new System.Drawing.Size(368, 212);
            this.lvFlights.TabIndex = 2;
            this.lvFlights.UseCompatibleStateImageBehavior = false;
            this.lvFlights.ColumnWidthChanging += new System.Windows.Forms.ColumnWidthChangingEventHandler(this.lvFlights_ColumnWidthChanging);
            this.lvFlights.SelectedIndexChanged += new System.EventHandler(this.lvFlights_SelectedIndexChanged);
            // 
            // numFlight
            // 
            this.numFlight.Text = "№";
            this.numFlight.Width = 23;
            // 
            // otkuda
            // 
            this.otkuda.Text = "Пункт отправления";
            this.otkuda.Width = 83;
            // 
            // kuda
            // 
            this.kuda.Text = "Пункт прибытия";
            this.kuda.Width = 83;
            // 
            // timeDep
            // 
            this.timeDep.Text = "Отправление";
            this.timeDep.Width = 85;
            // 
            // timeArr
            // 
            this.timeArr.Text = "Прибытие";
            this.timeArr.Width = 85;
            // 
            // gbFlight
            // 
            this.gbFlight.Controls.Add(this.tbFlight);
            this.gbFlight.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.gbFlight.Location = new System.Drawing.Point(3, 252);
            this.gbFlight.Name = "gbFlight";
            this.gbFlight.Size = new System.Drawing.Size(368, 274);
            this.gbFlight.TabIndex = 0;
            this.gbFlight.TabStop = false;
            this.gbFlight.Text = "Описание рейса";
            // 
            // tbFlight
            // 
            this.tbFlight.BackColor = System.Drawing.SystemColors.Window;
            this.tbFlight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbFlight.Location = new System.Drawing.Point(3, 16);
            this.tbFlight.Multiline = true;
            this.tbFlight.Name = "tbFlight";
            this.tbFlight.ReadOnly = true;
            this.tbFlight.Size = new System.Drawing.Size(362, 255);
            this.tbFlight.TabIndex = 0;
            this.tbFlight.Text = "Информация по рейсу";
            // 
            // msFlight
            // 
            this.msFlight.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmBlockFlight});
            this.msFlight.Location = new System.Drawing.Point(3, 16);
            this.msFlight.Name = "msFlight";
            this.msFlight.Size = new System.Drawing.Size(368, 24);
            this.msFlight.TabIndex = 1;
            this.msFlight.Text = "msFlight";
            // 
            // tsmBlockFlight
            // 
            this.tsmBlockFlight.Name = "tsmBlockFlight";
            this.tsmBlockFlight.Size = new System.Drawing.Size(77, 20);
            this.tsmBlockFlight.Text = "Взять рейс";
            this.tsmBlockFlight.Click += new System.EventHandler(this.tsmBlockFlight_Click);
            // 
            // gbPlanes
            // 
            this.gbPlanes.Controls.Add(this.lvPlanes);
            this.gbPlanes.Controls.Add(this.gbPlane);
            this.gbPlanes.Controls.Add(this.msPlane);
            this.gbPlanes.Dock = System.Windows.Forms.DockStyle.Left;
            this.gbPlanes.Location = new System.Drawing.Point(374, 24);
            this.gbPlanes.Name = "gbPlanes";
            this.gbPlanes.Size = new System.Drawing.Size(262, 529);
            this.gbPlanes.TabIndex = 3;
            this.gbPlanes.TabStop = false;
            this.gbPlanes.Text = "Самолёты игрока";
            // 
            // lvPlanes
            // 
            this.lvPlanes.Dock = System.Windows.Forms.DockStyle.Top;
            this.lvPlanes.FormattingEnabled = true;
            this.lvPlanes.Location = new System.Drawing.Point(3, 40);
            this.lvPlanes.Name = "lvPlanes";
            this.lvPlanes.Size = new System.Drawing.Size(256, 212);
            this.lvPlanes.TabIndex = 7;
            this.lvPlanes.SelectedIndexChanged += new System.EventHandler(this.lvPlanes_SelectedIndexChanged);
            // 
            // gbPlane
            // 
            this.gbPlane.Controls.Add(this.tbPlane);
            this.gbPlane.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.gbPlane.Location = new System.Drawing.Point(3, 252);
            this.gbPlane.Name = "gbPlane";
            this.gbPlane.Size = new System.Drawing.Size(256, 274);
            this.gbPlane.TabIndex = 2;
            this.gbPlane.TabStop = false;
            this.gbPlane.Text = "Описание самолёта";
            // 
            // tbPlane
            // 
            this.tbPlane.BackColor = System.Drawing.SystemColors.Window;
            this.tbPlane.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbPlane.Location = new System.Drawing.Point(3, 16);
            this.tbPlane.Multiline = true;
            this.tbPlane.Name = "tbPlane";
            this.tbPlane.ReadOnly = true;
            this.tbPlane.Size = new System.Drawing.Size(250, 255);
            this.tbPlane.TabIndex = 1;
            this.tbPlane.Text = "Информация о самолетах игрока";
            // 
            // msPlane
            // 
            this.msPlane.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmBlockPlane,
            this.tsmPlanes});
            this.msPlane.Location = new System.Drawing.Point(3, 16);
            this.msPlane.Name = "msPlane";
            this.msPlane.Size = new System.Drawing.Size(256, 24);
            this.msPlane.TabIndex = 3;
            this.msPlane.Text = "msPlane";
            // 
            // tsmBlockPlane
            // 
            this.tsmBlockPlane.Name = "tsmBlockPlane";
            this.tsmBlockPlane.Size = new System.Drawing.Size(126, 20);
            this.tsmBlockPlane.Text = "Назначить самолёт";
            this.tsmBlockPlane.Click += new System.EventHandler(this.tsmBlockPlane_Click);
            // 
            // tsmPlanes
            // 
            this.tsmPlanes.Name = "tsmPlanes";
            this.tsmPlanes.Size = new System.Drawing.Size(76, 20);
            this.tsmPlanes.Text = "Самолёты";
            this.tsmPlanes.Click += new System.EventHandler(this.tsmPlanes_Click);
            // 
            // gbStates
            // 
            this.gbStates.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbStates.Controls.Add(this.lvState);
            this.gbStates.Controls.Add(this.gbState);
            this.gbStates.Controls.Add(this.msState);
            this.gbStates.Location = new System.Drawing.Point(636, 24);
            this.gbStates.Name = "gbStates";
            this.gbStates.Size = new System.Drawing.Size(653, 529);
            this.gbStates.TabIndex = 4;
            this.gbStates.TabStop = false;
            this.gbStates.Text = "Рейсы на исполнении";
            // 
            // lvState
            // 
            this.lvState.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.num,
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader5,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader6});
            this.lvState.Dock = System.Windows.Forms.DockStyle.Top;
            this.lvState.Location = new System.Drawing.Point(3, 40);
            this.lvState.Name = "lvState";
            this.lvState.Size = new System.Drawing.Size(647, 212);
            this.lvState.TabIndex = 4;
            this.lvState.UseCompatibleStateImageBehavior = false;
            this.lvState.ColumnWidthChanging += new System.Windows.Forms.ColumnWidthChangingEventHandler(this.lvState_ColumnWidthChanging);
            this.lvState.SelectedIndexChanged += new System.EventHandler(this.lvState_SelectedIndexChanged);
            // 
            // num
            // 
            this.num.Text = "№";
            this.num.Width = 25;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Пункт отправления";
            this.columnHeader1.Width = 83;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Пункт прибытия";
            this.columnHeader2.Width = 83;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Вид рейса";
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Дата отправления";
            this.columnHeader3.Width = 89;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Дата прибытия";
            this.columnHeader4.Width = 89;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Назначенный самолет";
            // 
            // gbState
            // 
            this.gbState.Controls.Add(this.tbState);
            this.gbState.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.gbState.Location = new System.Drawing.Point(3, 252);
            this.gbState.Name = "gbState";
            this.gbState.Size = new System.Drawing.Size(647, 274);
            this.gbState.TabIndex = 2;
            this.gbState.TabStop = false;
            this.gbState.Text = "Состояние рейса";
            // 
            // tbState
            // 
            this.tbState.BackColor = System.Drawing.SystemColors.Window;
            this.tbState.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbState.Location = new System.Drawing.Point(3, 16);
            this.tbState.Multiline = true;
            this.tbState.Name = "tbState";
            this.tbState.ReadOnly = true;
            this.tbState.Size = new System.Drawing.Size(641, 255);
            this.tbState.TabIndex = 1;
            this.tbState.Text = "Состояние Ваших рейсов";
            // 
            // msState
            // 
            this.msState.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmCancelPlane,
            this.tsmCancelFlight});
            this.msState.Location = new System.Drawing.Point(3, 16);
            this.msState.Name = "msState";
            this.msState.Size = new System.Drawing.Size(647, 24);
            this.msState.TabIndex = 3;
            this.msState.Text = "msState";
            // 
            // tsmCancelPlane
            // 
            this.tsmCancelPlane.Name = "tsmCancelPlane";
            this.tsmCancelPlane.Size = new System.Drawing.Size(117, 20);
            this.tsmCancelPlane.Text = "Отозвать самолёт";
            this.tsmCancelPlane.Click += new System.EventHandler(this.tsmCancelPlane_Click);
            // 
            // tsmCancelFlight
            // 
            this.tsmCancelFlight.Name = "tsmCancelFlight";
            this.tsmCancelFlight.Size = new System.Drawing.Size(102, 20);
            this.tsmCancelFlight.Text = "Отменить рейс";
            this.tsmCancelFlight.Click += new System.EventHandler(this.tsmCancelFlight_Click);
            // 
            // GameTimer
            // 
            this.GameTimer.Enabled = true;
            this.GameTimer.Interval = 1000;
            this.GameTimer.Tick += new System.EventHandler(this.GameTimer_Tick);
            // 
            // SecTimer
            // 
            this.SecTimer.Interval = 1000;
            this.SecTimer.Tick += new System.EventHandler(this.SecTimer_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::madus.Properties.Resources.airplane;
            this.ClientSize = new System.Drawing.Size(1289, 577);
            this.Controls.Add(this.gbStates);
            this.Controls.Add(this.gbPlanes);
            this.Controls.Add(this.gbFlights);
            this.Controls.Add(this.msMenu);
            this.Controls.Add(this.msTime);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.msMenu;
            this.Name = "MainForm";
            this.Text = "Аэропорт";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.msMenu.ResumeLayout(false);
            this.msMenu.PerformLayout();
            this.msTime.ResumeLayout(false);
            this.msTime.PerformLayout();
            this.gbFlights.ResumeLayout(false);
            this.gbFlights.PerformLayout();
            this.gbFlight.ResumeLayout(false);
            this.gbFlight.PerformLayout();
            this.msFlight.ResumeLayout(false);
            this.msFlight.PerformLayout();
            this.gbPlanes.ResumeLayout(false);
            this.gbPlanes.PerformLayout();
            this.gbPlane.ResumeLayout(false);
            this.gbPlane.PerformLayout();
            this.msPlane.ResumeLayout(false);
            this.msPlane.PerformLayout();
            this.gbStates.ResumeLayout(false);
            this.gbStates.PerformLayout();
            this.gbState.ResumeLayout(false);
            this.gbState.PerformLayout();
            this.msState.ResumeLayout(false);
            this.msState.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip msMenu;
        private System.Windows.Forms.ToolStripMenuItem tsmExample;
        private System.Windows.Forms.ToolStripMenuItem работаСоВременемToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmSlow;
        private System.Windows.Forms.ToolStripMenuItem tsmPlay;
        private System.Windows.Forms.ToolStripMenuItem tsmFast;
        private System.Windows.Forms.GroupBox gbFlights;
        private System.Windows.Forms.GroupBox gbPlanes;
        private System.Windows.Forms.GroupBox gbStates;
        private System.Windows.Forms.GroupBox gbFlight;
        private System.Windows.Forms.GroupBox gbPlane;
        private System.Windows.Forms.GroupBox gbState;
        private System.Windows.Forms.ToolStripMenuItem tsmHelp;
        private System.Windows.Forms.TextBox tbFlight;
        private System.Windows.Forms.TextBox tbPlane;
        private System.Windows.Forms.TextBox tbState;
        private System.Windows.Forms.ToolStripMenuItem tsmExit;
        private System.Windows.Forms.Timer GameTimer;
        private System.Windows.Forms.ToolStripMenuItem tsmStartGame;
        private System.Windows.Forms.ToolStripMenuItem tsmLoadGame;
        private System.Windows.Forms.ToolStripMenuItem tsmSaveGame;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem tsmExitGame;
        private System.Windows.Forms.MenuStrip msFlight;
        private System.Windows.Forms.ToolStripMenuItem tsmBlockFlight;
        private System.Windows.Forms.MenuStrip msPlane;
        private System.Windows.Forms.ToolStripMenuItem tsmBlockPlane;
        private System.Windows.Forms.ToolStripMenuItem tsmPlanes;
        private System.Windows.Forms.MenuStrip msState;
        private System.Windows.Forms.ToolStripMenuItem tsmSeparator;
        private System.Windows.Forms.ToolStripMenuItem tsmSchet;
        public System.Windows.Forms.ToolStripMenuItem tsmTimer;
        public System.Windows.Forms.MenuStrip msTime;
        public System.Windows.Forms.ToolStripMenuItem tsmMoney;
        private System.Windows.Forms.ListView lvFlights;
        private System.Windows.Forms.ColumnHeader numFlight;
        private System.Windows.Forms.ColumnHeader otkuda;
        private System.Windows.Forms.ListView lvState;
        private System.Windows.Forms.ColumnHeader kuda;
        private System.Windows.Forms.ColumnHeader timeDep;
        private System.Windows.Forms.ColumnHeader timeArr;
        private System.Windows.Forms.ColumnHeader num;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ListBox lvPlanes;
        private System.Windows.Forms.ToolStripMenuItem tsmCancelPlane;
        private System.Windows.Forms.ToolStripMenuItem tsmCancelFlight;
        private System.Windows.Forms.ToolStripMenuItem tsmInfo;
        private System.Windows.Forms.Timer SecTimer;
        private System.Windows.Forms.ToolStripMenuItem tsmFuel;
        private System.Windows.Forms.ToolStripMenuItem tsmFuelPrice;
        private System.Windows.Forms.ToolStripMenuItem tsmHiring;
        private System.Windows.Forms.ToolStripMenuItem tsmActions;
        private System.Windows.Forms.ToolStripMenuItem tsmImage;
        private System.Windows.Forms.ToolStripMenuItem tsmImageValue;
        private System.Windows.Forms.ToolStripMenuItem tsmCompetitorImage;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
    }
}