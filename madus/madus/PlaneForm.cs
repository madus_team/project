﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace madus
{
    public partial class PlaneForm : Form
    {
        MainForm mainform;
        public PlaneForm(MainForm mainform)
        {
            InitializeComponent();
            tbMaker.Text = "";
            tbPlane.Text = "";
            tbInfo.Visible = false;
            this.mainform = mainform;
            updatePlaneLB();

        }
        private void updatePlaneLB()
        {
            lbMaker.Items.Clear();
            lvPlanes.Items.Clear();
            //mainform.lvPlanes.Items.Clear();
           // mainform.lvPlanes.Clear();
            for (int i = 0; i < mainform.userPlanes.Count; i++)
                if (!usedPlane(mainform.userPlanes[i].idAirplane)) // используемые самолеты не даем продавать
                    lvPlanes.Items.Add(mainform.userPlanes[i].namePlane);
            for (int i = 0; i < mainform.buyedPlanes.Count; i++)
                lvPlanes.Items.Add(mainform.buyedPlanes[i].namePlane + " (Идет сборка)");
            for (int i = mainform.userPlanes.Count; i < lvPlanes.Items.Count; i++)
                lvPlanes.Items[i].ForeColor = Color.Red;
            for (int i = 0; i < mainform.shopPlanes.Count; i++)
                lbMaker.Items.Add(mainform.shopPlanes[i].namePlane);

            tbMoney.Text = mainform.money.ToString();

        }

        private bool usedPlane(int idPlane) // true, если самолет назначен на какой-то рейс
        {
            for (int i = 0; i < mainform.userFlights.Count; i++)
                if (mainform.userFlights[i].IDPlane == idPlane)
                    return true;
            return false;
        }
        
        private void btnBuy_Click(object sender, EventArgs e)
        {
            int selectLine = lbMaker.SelectedIndex;
            if (selectLine == -1)
            {
                MessageBox.Show("Не выбран самолет для покупки");
                return;
            }

            if (mainform.money - mainform.shopPlanes[selectLine].aircraftCost < 0)
            {
                MessageBox.Show("Нехватает средств для покупки самолета!");
                return;
            }
           
            DialogResult result = MessageBox.Show("Ваш счет: " + mainform.money + Environment.NewLine + "Стоимость самолета: " +
            mainform.shopPlanes[selectLine].aircraftCost.ToString() + Environment.NewLine +
            "Вы хотите купить этот самолет?", "Покупка самолета", MessageBoxButtons.YesNo, MessageBoxIcon.None);

            if (result == DialogResult.Yes)
            {
                    mainform.money -= mainform.shopPlanes[selectLine].aircraftCost;

                UpPlanesCost(mainform.shopPlanes[selectLine].namePlane);
                mainform.shopPlanes[selectLine].aircraftCost *= 0.6;
                    mainform.buyedPlanes.Add(mainform.shopPlanes[selectLine]);
                    mainform.shopPlanes.RemoveAt(selectLine);


                    updatePlaneLB();
             }
           
        }

        private void UpPlanesCost(string NamePlane)
        {
            foreach (var plane in mainform.planes)
            {
                if (plane[1] == NamePlane)
                {
                    plane[4] = (Convert.ToDouble(plane[4]) * 1.02).ToString();
                }
            }
        }

        private void buttonPlane(Airplane plain)//метод который выбирает какие кнопки дизаблить
        {
            if (plain.rent)
            {
                btnSale.Text = "Отмена аренды";
                
                return;
            }
            if (!plain.rent&& !plain.leasing)
            {
                btnSale.Text ="Продажа";
                
                return;
            }
            if (plain.leasing)
            {
                btnSale.Text= "Отмена лизинга";

                return;
            }

        }
        private void lvPlanes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvPlanes.SelectedIndices.Count == 0)
            {
                tbPlane.Text = "";
                return;
            }
            tbPlane.Clear();
            int numberPlanes = lvPlanes.SelectedIndices[0];

            tbPlane.Text = mainform.outputAirplane(numberPlanes);
            if (numberPlanes >= mainform.userPlanes.Count)
            {
                numberPlanes -= mainform.userPlanes.Count;
                buttonPlane(mainform.buyedPlanes[numberPlanes]);
            }
            else
                buttonPlane(mainform.userPlanes[numberPlanes]);
        }
        
        private void lbMaker_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbMaker.SelectedIndex != -1)
            {
                
                tbMaker.Clear();
                int numberPlanes = lbMaker.SelectedIndex;
                tbMaker.Text = mainform.shopPlanes[numberPlanes].Info() + Environment.NewLine +
                "Цена аренды за 1 день: " + mainform.shopPlanes[numberPlanes].rentPrice;
            }
            else
            {
                tbMaker.Text = "";
            }
        }

        private void btnArenda_Click(object sender, EventArgs e)
        {
            int selectLine = lbMaker.SelectedIndex;
            if (selectLine == -1)
            {
                MessageBox.Show("Не выбран самолет для аренды");
                return;
            }

            if (mainform.money - mainform.shopPlanes[selectLine].rentPrice < 0)
            {
                MessageBox.Show("Нехватает средств для аренды самолета!");
                return;
            }

            DialogResult result = MessageBox.Show("Ваш счет: " + mainform.money + Environment.NewLine + "Стоимость аренды самолета на 1 день: " +
            mainform.shopPlanes[selectLine].rentPrice.ToString() + Environment.NewLine +
            "Вы хотите арендовать этот самолет ?", "Аренда самолета", MessageBoxButtons.YesNo, MessageBoxIcon.None);

            if (result == DialogResult.Yes)
            {
                MessageBox.Show("Ваш самолет отправлен на сборку. Платеж совершается еждневно, пока вы не совершите отмену аренды.");
               
                //деньги берем на след день как он соберется 
                mainform.shopPlanes[selectLine].rent = true;
                DateTime ndt = DateTime.Parse(mainform.tsmTimer.Text);
                mainform.shopPlanes[selectLine].startDate = new DateTime(ndt.Year,ndt.Month,ndt.Day);
                mainform.buyedPlanes.Add(mainform.shopPlanes[selectLine]);
                mainform.shopPlanes.RemoveAt(selectLine);
                updatePlaneLB();
            }
        }

        private void btnSale_Click(object sender, EventArgs e)
        {
            if (lvPlanes.SelectedIndices.Count == 0)
                return;
            DialogResult result = MessageBox.Show("Вы хотите продать этот самолет?", "Продажа самолета", MessageBoxButtons.YesNo, MessageBoxIcon.None);

            if (result == DialogResult.Yes)
            {
                int numberPlanes = lvPlanes.SelectedIndices[0];
                if (numberPlanes >= mainform.userPlanes.Count)
                {
                    numberPlanes -= mainform.userPlanes.Count;
                    //за лизинг в первый день не берём
                    if (!mainform.buyedPlanes[numberPlanes].leasing)
                    mainform.money += mainform.buyedPlanes[numberPlanes].aircraftCost;
                    mainform.buyedPlanes.RemoveAt(numberPlanes);
                    updatePlaneLB();
                    return;
                }
                else if(mainform.userPlanes[numberPlanes].rent)
                {
                    if (numberPlanes >= mainform.userPlanes.Count)
                    {
                        numberPlanes -= mainform.userPlanes.Count;
                        mainform.buyedPlanes.RemoveAt(numberPlanes);
                        updatePlaneLB();
                        return;
                    }
                    mainform.userPlanes.RemoveAt(numberPlanes);
                    updatePlaneLB();
                    return;
                }
                else if(mainform.userPlanes[numberPlanes].leasing)
                {
                    TimeSpan time = DateTime.Parse(mainform.tsmTimer.Text)-mainform.userPlanes[numberPlanes].startDate;
                    mainform.money += time.Days * mainform.userPlanes[numberPlanes].rentPrice*0.6;
                    mainform.userPlanes.RemoveAt(numberPlanes);
                    updatePlaneLB();
                    return;
                }
                mainform.money += mainform.userPlanes[numberPlanes].aircraftCost;
                mainform.userPlanes[numberPlanes].ReleaseCrew(); //освобождение экипажа
                mainform.userPlanes.RemoveAt(numberPlanes);
                updatePlaneLB();
                
            }
        }

        private void gbOperations_Enter(object sender, EventArgs e)
        {

        }

        private void tbLeasing_Click(object sender, EventArgs e)
        {
            tbInfo.Visible = true;
        }

        private void btnLising_Click(object sender, EventArgs e)
        {
            int selectLine = lbMaker.SelectedIndex;
            if (selectLine == -1)
            {
                MessageBox.Show("Не выбран самолет для лизинга");
                return;
            }

           /* if (mainform.money - mainform.shopPlanes[selectLine].rentPrice < 0)
            {
                MessageBox.Show("Нехватает средств для лизинга самолета!");
                return;
            }*/
            DateTime dtSub = DateTime.Parse(mainform.tsmTimer.Text);
            mainform.shopPlanes[selectLine].startDate = new DateTime(dtSub.Year, dtSub.Month, dtSub.Day);
            mainform.shopPlanes[selectLine].leasingEnd_Calc((int)numLeasing.Value);

            DialogResult result = MessageBox.Show("Ваш счет: " + mainform.money + Environment.NewLine + 
            "Стоимость лизинга самолета (в день): " +
            mainform.shopPlanes[selectLine].rentPrice.ToString() + Environment.NewLine +
            "Стоимость лизинга самолета (по истечении срока): " +
            mainform.shopPlanes[selectLine].leasingPrice.ToString() + Environment.NewLine +
            "Вы хотите взять этот самолет на лизинг?", "Лизинг самолета", MessageBoxButtons.YesNo, MessageBoxIcon.None);

            if (result == DialogResult.Yes)
            {
                mainform.buyedPlanes.Add(mainform.shopPlanes[selectLine]);
                mainform.buyedPlanes[mainform.buyedPlanes.Count - 1].leasing = true;
                mainform.buyedPlanes[mainform.buyedPlanes.Count - 1].rent = false;
                mainform.buyedPlanes[mainform.buyedPlanes.Count - 1].aircraftCost *= 0.6;
                mainform.shopPlanes.RemoveAt(selectLine);
                updatePlaneLB();
            }
        }

        private void tbPlane_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnArendaNo_Click(object sender, EventArgs e)
        {
            int selectLine = lbMaker.SelectedIndex;
            if (selectLine == -1)
            {
                MessageBox.Show("Вы не выбрали самолет");
                return;
            }

        }
    }
}
