﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace madus
{
    public enum AgeType //Возраст самолёта (новый/устаревающий/старый)
    {
        Newbie = 1,
        Obsolete = 2,
        Elder = 3
    }
    public class Airplane
    {
        public double ObsoleteDistance = 5000; //дистанция, после которой самолёт считается устаревающим
        public double ElderDistance = 10000; //дистанция, после которой самолёт считается старым


        private FlightType TypePlane; // тип самолета (грузовой, пассажирский)
        private AgeType AgeTypePlane; // возраст самолёта
        private double AgeDistance; //километраж самолёта, то есть сколько он пролетел
        private string NamePlane;//Название самолета*
        private int Capacity;//вместимость*
        private double AircraftCost;//стоимость самолета*
        private double RentPrice;//цена за аренду* 
        private double LeasingPrice;//цена за лизинг, номинальная
        private double MaintenancePrice; //цена обслуживания
        private double Distance;//доступная дальность*
        private int IdAirplane;//id самолета*
        private Airport _airport;//аэропорт, где находиться самолет
        private bool Rent;
        private bool Leasing;
        private bool ToRemove;
        private bool IdBuyer;//id владельца
        private DateTime StartDate = new DateTime();//дата взятия аренды
                                   //    private DateTime EndDate;//дата окончания аренды
        private DateTime LeasingEndDate = new DateTime();//дата окончания лизинга
        private double costs;//расходы на 1 км
        public bool ToExecute = false;
        private bool InAir = false;
        private double Сonsumption;

        public Airplane(FlightType TypePlane, string NamePlane,int Capacity,double AircraftCost,double Distance,int IdAirplane, double AgeDistance) 
        {
            this.TypePlane = TypePlane;
            this.AgeTypePlane = AgeType.Newbie; //Изначальное состояние хорошее
            this.AgeDistance = AgeDistance;
            this.NamePlane = NamePlane;
            this.Capacity = Capacity;
            this.Distance = Distance;
            this.AircraftCost = AircraftCost;
            this.MaintenancePrice = AircraftCost / 100;
            this.RentPrice = AircraftCost / 100;
            this.IdAirplane = IdAirplane;
            this._airport = new Airport(new Airport.Coordinates(-1.0, -1.0), "none");
            this.ToRemove = false;
            this.Costs = 2.5 * 0.7 * this.Capacity;
            if (this.TypePlane == FlightType.Cargo) this.Costs /= 1000;
            if (this.TypePlane == FlightType.Cargo) 
                this.consumption = 10;
            else
                this.consumption = 7;
        }
        public Airplane()
        { }
        public string Info()
        {
            string infoPlane;
            infoPlane = "Номер самолёта: " + idAirplane;
            if (TypePlane.ToString() == "Passenger")
            {
                infoPlane += Environment.NewLine + "Тип: пассажирский " + Environment.NewLine + "Название: " + NamePlane + Environment.NewLine +
                "Вместимость: " + Capacity;
            }
            else
            {
                infoPlane += Environment.NewLine + "Тип: грузовой " + Environment.NewLine;
                infoPlane += "Название: " + NamePlane + Environment.NewLine +
                "Грузоподъемность: " + Capacity;
            }
            infoPlane += Environment.NewLine + "Состояние самолёта: ";
            switch (AgeTypePlane)
            {
                case AgeType.Newbie:
                    {
                        infoPlane += "Новый "; break; }
                case AgeType.Obsolete:
                    {
                        infoPlane += "Устаревающий "; break; }
                case AgeType.Elder:
                    {
                        infoPlane += "Старый "; break; }
                default:
                    {
                        infoPlane += "Неопределённое "; break;
                    }
            }
            infoPlane+= Environment.NewLine + "Максимальная дальность полета: " +
            Distance.ToString();
            infoPlane += Environment.NewLine + "Аэропорт местонахождения: " + _airport.AirportName;
            if (Rent)
                infoPlane += Environment.NewLine + "Самолет взят в аренду с " + StartDate.ToString() +
                Environment.NewLine + "Цена за день: " + RentPrice.ToString();
            else if (Leasing)
                infoPlane += Environment.NewLine + "Самолет взят на лизинг" + 
                Environment.NewLine + " с " + StartDate.ToString() +
                Environment.NewLine + "до " + LeasingEndDate.ToString() +
                Environment.NewLine + "Цена за день: " + RentPrice.ToString() +
                Environment.NewLine + "Цена покупки: " + LeasingPrice.ToString();
            else
                infoPlane += Environment.NewLine + "Стоимость самолета: " + AircraftCost.ToString();
            infoPlane += Environment.NewLine + "Стоимость обслуживания: " + MaintenancePrice.ToString();
            infoPlane += Environment.NewLine + "Расход топлива: " + consumption.ToString() + " л/км";
            return infoPlane;
        }

        public void updateAircraftCost() //ВЫЗЫВАТЬ ПОСЛЕ КАЖДОГО РЕЙСА! происходит вычисление возраста самолёта по пройденной дистанции
        {
            if (ageDistance > ObsoleteDistance && this.AgeTypePlane == AgeType.Newbie)
            {
                this.AgeTypePlane = AgeType.Obsolete;
                this.AircraftCost = this.AircraftCost * 0.7;
                this.MaintenancePrice = this.MaintenancePrice * 1.3;
            }
            if (ageDistance > ElderDistance && this.AgeTypePlane != AgeType.Elder)
            {
                this.AgeTypePlane = AgeType.Elder;
                this.AircraftCost = this.AircraftCost * 0.7;
                this.MaintenancePrice = this.MaintenancePrice * 1.3;
            }
            
        }
        public double aircraftCost
        {
            get { return AircraftCost; }

            set { AircraftCost = value; }//оставлю. возможно в процессе игры надо будет изменить цену
        }

        public double maintenancePrice
        {
            get { return MaintenancePrice; }
        }

        public int сapacity        { get { return Capacity; } }
        public AgeType ageTypePlane
        {
            get { return AgeTypePlane; }

            set { AgeTypePlane = value; }
        }
        public double ageDistance
        {
            get { return AgeDistance; }

            set { AgeDistance = value; }
        }
        public FlightType typePlane { get { return TypePlane; } }
        public string namePlane { get { return NamePlane; } }
        public DateTime startDate
        {
            get { return StartDate; }

            set { StartDate = value; }
        }
        public DateTime leasingEndDate//дата окончания лизинга
        {
            get { return LeasingEndDate; }

            set { LeasingEndDate = value; }
        }
        public void leasingEnd_Calc(int days) //для лизинга
        {
            LeasingEndDate = StartDate.AddDays(days);
            LeasingPrice = AircraftCost - RentPrice * (days)/2.0;
        }
        
        //public DateTime endDate
        //{
        //    get { return EndDate; }

        //    set { EndDate = value; }// как задается конец аренды
        //}
        public double rentPrice
        {
            get { return RentPrice; }
            
        }
        public double leasingPrice
        {
            get { return LeasingPrice; }

        }
        public double distance
        {
            get { return Distance; }

          
        }
        public bool rent
        {
            get { return Rent; }

            set { Rent = value; }
        }
        public bool toRemove
        {
            get { return ToRemove; }

            set { ToRemove = value; }
        }
        public bool leasing
        {
            get { return Leasing; }

            set { Leasing = value; }
        }
        public int idAirplane
        {
            get { return IdAirplane; }

        
        }
        public Airport curAirport
        {
            get { return _airport; }

            set { _airport = value; }
        }
        public bool idBuyer
        {
            get { return IdBuyer; }

            set { IdBuyer = value; }
        }
        public double Costs
        {
            get { return costs; }

            set { costs = value; }
        }

        public bool inAir
        {
            get { return InAir; }
            set { InAir = value; }
        }

<<<<<<< HEAD
        public double consumption
        {
            get { return Сonsumption; }
            set { Сonsumption = value; }
=======
        /// <summary>
        /// Метод, определяющий успеет ли самолет прибыть (из текущего) в нужный аэропорт за нужное время
        /// </summary>
        /// <returns>
        /// </returns>
        public bool IsEnableToArriveInTime(Airport AirportTo, DateTime TimeNow, DateTime Depature)
        {
            bool isenable = false;                                      
            if (_airport.AirportName == AirportTo.AirportName)
                return true;
            double distance = _airport.GetDistanceBeetwen(AirportTo.AirPoint);
            double hour_time = distance / 1000;
            if (TimeNow + TimeSpan.FromHours(hour_time) > Depature)
                isenable = false;
            else
                isenable = true;

            return isenable;
>>>>>>> b8cdc352cee506ab218abd791040c2579e75edc0
        }
    }
}
