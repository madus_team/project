﻿namespace madus
{
    partial class ToCrewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.numSalary = new System.Windows.Forms.NumericUpDown();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.numWorkers = new System.Windows.Forms.NumericUpDown();
            this.lblNumOfWorkers = new System.Windows.Forms.Label();
            this.lblSalary = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numSalary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWorkers)).BeginInit();
            this.SuspendLayout();
            // 
            // numSalary
            // 
            this.numSalary.Location = new System.Drawing.Point(101, 38);
            this.numSalary.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numSalary.Name = "numSalary";
            this.numSalary.Size = new System.Drawing.Size(83, 20);
            this.numSalary.TabIndex = 4;
            this.numSalary.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numSalary.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // btnOk
            // 
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Location = new System.Drawing.Point(12, 64);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(83, 23);
            this.btnOk.TabIndex = 5;
            this.btnOk.Text = "Начать";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(101, 64);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(83, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // numWorkers
            // 
            this.numWorkers.Location = new System.Drawing.Point(101, 12);
            this.numWorkers.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numWorkers.Name = "numWorkers";
            this.numWorkers.Size = new System.Drawing.Size(83, 20);
            this.numWorkers.TabIndex = 7;
            this.numWorkers.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblNumOfWorkers
            // 
            this.lblNumOfWorkers.AutoSize = true;
            this.lblNumOfWorkers.Location = new System.Drawing.Point(1, 14);
            this.lblNumOfWorkers.Name = "lblNumOfWorkers";
            this.lblNumOfWorkers.Size = new System.Drawing.Size(98, 13);
            this.lblNumOfWorkers.TabIndex = 8;
            this.lblNumOfWorkers.Text = "Требуется людей:";
            // 
            // lblSalary
            // 
            this.lblSalary.AutoSize = true;
            this.lblSalary.Location = new System.Drawing.Point(36, 40);
            this.lblSalary.Name = "lblSalary";
            this.lblSalary.Size = new System.Drawing.Size(63, 13);
            this.lblSalary.TabIndex = 9;
            this.lblSalary.Text = "Зар.Плата:";
            // 
            // ToCrewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(189, 94);
            this.Controls.Add(this.lblSalary);
            this.Controls.Add(this.lblNumOfWorkers);
            this.Controls.Add(this.numWorkers);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.numSalary);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ToCrewForm";
            this.Text = "Объявление о наборе нового персонала";
            ((System.ComponentModel.ISupportInitialize)(this.numSalary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWorkers)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown numSalary;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.NumericUpDown numWorkers;
        private System.Windows.Forms.Label lblNumOfWorkers;
        private System.Windows.Forms.Label lblSalary;
    }
}